//
//  SelectTIpTableViewCell.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 9/21/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

class SelectTipTableViewCell: UITableViewCell {
    
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var tipView: UIView!
    
    var tip: Double?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tipView.layer.cornerRadius = 10
        tipView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func refreshCell() {
        for button in buttonStackView.subviews {
            if let tipButton = button as? UIButton {
                guard let value = Int(String(tipButton.title(for: .normal)!.prefix(while: { char in char != "%"}))) else {
                    continue
                }
                let isTip = Int(tip ?? -2) == value
                tipButton.backgroundColor = isTip ? Colors.primaryColor : Colors.unselected
                tipButton.clipsToBounds = true
                tipButton.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
    
}
