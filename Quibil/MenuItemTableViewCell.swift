//
//  MenuItemTableViewCell.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/25/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    var item: Item? {
        didSet {
            itemTitleLabel.text = item?.name
            if let price = item?.price { priceLabel.text = "$\(String(format: "%.2f", price))" }
            else { priceLabel.text = "" }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
