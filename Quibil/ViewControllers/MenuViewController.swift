//
//  MenuViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/25/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MenuViewController: UIViewController {
    
    @IBOutlet weak var placeTitle: UILabel!
    @IBOutlet weak var tabStackView: UIStackView!
    @IBOutlet weak var pageView: UIView!
    @IBOutlet weak var itemTableView: UITableView!
    @IBOutlet weak var currentTabLabel: UILabel!
    @IBOutlet weak var cartButtonHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    
    var datasource: MealSesssionDataSource?
    var viewModel: MenuViewViewModel?
    
    var place: Place? {
        get {
            return datasource?.mealSession(for: self)?.place
        }
    }
    
    var menu: Menu? {
        get {
            return place?.menu
        }
    }
    
    var currentCategory: Int = 0 {
        didSet {
            if currentCategory != oldValue {
                itemTableView.reloadData()
            }
        }
    }
    
    var sessionHandler: UInt?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemTableView.dataSource = self
        itemTableView.delegate = self
        itemTableView.rowHeight = UITableView.automaticDimension
        itemTableView.tableFooterView = UIView()
        itemTableView.register(
            UINib(nibName: "MenuItemTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuItemTableViewCell")
        
        placeTitle.text = place?.name
        for i in 0..<(menu?.categories.count ?? 0) {
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 45) )
            button.setTitle(menu?.categories[i].name, for: .normal)
            button.setTitleColor(UIColor.black, for: .normal)
            button.tag = i
            button.addTarget(self, action: #selector(tabSelected), for: .touchUpInside)
            tabStackView.addArrangedSubview(button)
        }
        
        sessionHandler = APP.app.currentSession?.listenToOrders(onTrigger: {
            print("Session Changed")
            self.refresh()
        })
        
        print(place != nil)
        refresh()
    }
    
    private func setUpViewModel() {
        guard let menu = self.menu else {
            print("No menu found")
            self.showError(title: "Error", message: "No Menu Found.", buttonText: "Close", completion: nil)
            return
        }
        self.viewModel = MenuViewViewModel(menu: menu)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func tabSelected(sender: UIButton) {
        currentCategory = sender.tag
        refresh()
    }
    
    func refresh() {
        for button in tabStackView.subviews {
            let color = (button as? UIButton)?.tag == currentCategory ? UIColor.black : UIColor.gray
            (button as? UIButton)?.setTitleColor(color, for: .normal)
        }
        // Does not show cart right now
        cartButtonHeightContraint.constant = (APP.app.currentSession?.orders.count ?? 0) > 0 ? 0 : 0
        let numItems = APP.app.currentSession?.orders.count ?? 0
        let numString = numItems > 0 ? "(\(numItems))" : ""
        currentTabLabel.text = "My Items \(numString)"
        self.view.layoutSubviews()
    }
    
    @IBAction func openCurrentTab(_ sender: Any) {
        let tab = PlacedOrdersViewController(nibName: "PlacedOrdersViewController", bundle: nil)
        self.navigationController?.pushViewController(tab, animated: true)
    }
    
    @IBAction func backToWelcome(_ sender: Any) {
        self.defaultDismiss()
    }
    
}

extension MenuViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return (menu?.categories.count ?? 0) > 0 ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu?.categories[currentCategory].items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = itemTableView.dequeueReusableCell(withIdentifier: "MenuItemTableViewCell", for: indexPath) as! MenuItemTableViewCell
        let item = menu?.categories[currentCategory].items[indexPath.row]
        cell.itemTitleLabel.text = item?.name
        if let price = item?.price {
            cell.priceLabel.text = "$\(String(format: "%.2f", price))"
        } else {
            cell.priceLabel.text = ""
        }
        return cell
    }
}

extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = FoodItemViewController()
        vc.item = menu?.categories[currentCategory].items[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
