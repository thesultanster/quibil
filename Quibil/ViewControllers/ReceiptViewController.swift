//
//  RecieptViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 8/8/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit
import BraintreeDropIn
import Braintree


class ReceiptViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var payButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    
    var payButtonHeight: CGFloat?
    var mealSession: MealSession?
    
    var tax: Double? = 9.75
    var tip: Double? = 15
    var sections = 0
    var isLoading: Bool = false {
        didSet {
            if oldValue != isLoading {
                if isLoading {
                    self.showLoader()
                } else {
                    self.removeLoader()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        payButtonHeightConstraint.constant = payButtonHeight ?? 45
        tableView.register(UINib(nibName: "ItemPriceTableTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemPriceTableTableViewCell")
        tableView.register(UINib(nibName: "SelectTipTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectTipTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.defaultDismiss()
    }
    
    @IBAction func payBill(_ sender: Any) {
        let tokenKey = APP.app.braintreeClientToken != nil
            ? APP.app.braintreeClientToken!
            : APP.app.BRAINTREE_KEY
        showDropIn(clientTokenOrTokenizationKey: tokenKey)
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
                self.dismiss(animated: true, completion: nil)
                self.showError(title: "Error", message: "Could not process payment.", buttonText: "Close", completion: nil)
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
                self.dismiss(animated: true, completion: nil)
            } else if let result = result {
                guard let method = result.paymentMethod else {
                    return
                }
                controller.dismiss(animated: true, completion: {
                    self.isLoading = true
                    APP.app.closeOutSession(paymentMethod: method, onSucces: { (error) in
                        print(error ?? "Unknown error")
                        self.isLoading = false
                        self.showError(title: "Thank You!", message: "Your payment is being processed", buttonText: "Close", completion: {
                            APP.app.currentSession = nil
                            print("Going back to Loading")
                            self.navigationController?.popToRootViewController(animated: true)
                        })
                    })
                })
            }
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
}

extension ReceiptViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        sections = 5 + (mealSession?.orders.count ?? 0)
        print(sections)
        return sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: // title
            return 1
        case sections - 4: // subtotal
            return 1
        case sections - 3: // tax
            return 1
        case sections - 2: // tip
            return mealSession?.closedOutAt != nil ? 1 : 2
        case sections - 1: // total
            return 1
        default: // orders
            print(section)
            return 1 + (mealSession?.orders[section - 1].addOns.count ?? 0)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemPriceTableTableViewCell", for: indexPath) as! ItemPriceTableTableViewCell
        cell.seperatorView.isHidden = true
        cell.itemNameIndent.constant = 10
        switch indexPath.section {
        case 0: // Title
            cell.itemNameLabel.text = "\(mealSession?.place.name ?? "")\nOrder"
            cell.priceLabel.text = "\nPrice"
            if let closeDate = mealSession?.closedOutAt {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "M/d/yyyy\nPrice"
                cell.priceLabel.text = dateFormatter.string(from: closeDate)
            }
        case sections - 4: // SubTotal
            cell.itemNameLabel.text = "Subtotal"
            cell.priceLabel.text = "$\(String(format: "%.2f", mealSession?.subTotal ?? 0))"
        case sections - 3: // Tax
            cell.itemNameLabel.text = "Sales Tax \(String(format: "%.2f", (mealSession?.tax ?? 0) * 100))%"
            cell.priceLabel.text = "$\(String(format: "%.2f", (mealSession?.totalTax ?? 0)))"
        case sections - 2: // Tip
            if mealSession?.closedOutAt == nil && indexPath.row == 1 {
                let tipCell = tableView.dequeueReusableCell(withIdentifier: "SelectTipTableViewCell", for: indexPath) as! SelectTipTableViewCell
                tipCell.tip = (mealSession?.tip ?? 0) * 100
                for button in tipCell.buttonStackView.subviews {
                    guard let title = (button as? UIButton)?.title(for: .normal) else {
                        continue
                    }
                    guard let value = Int(String(title.prefix(while: { char in char != "%"}))) else {
                        continue
                    }
                    button.tag = value
                    (button as? UIButton)?.addTarget(self, action: #selector(tipButtonPressed), for: .touchUpInside)
                }
                tipCell.refreshCell()
                return tipCell
            }
            cell.itemNameLabel.text = "Tip \(String(format: "%.0f", (mealSession?.tip ?? 0) * 100))%"
            cell.priceLabel.text = "$\(String(format: "%.2f", mealSession?.totalTip ?? 0))"
        case sections - 1: // Total
            cell.seperatorView.isHidden = false
            cell.itemNameLabel.text = "Total"
            cell.priceLabel.text = "$\(String(format: "%.2f", mealSession?.total ?? 0))"
        default:
            // orders
            guard let order = mealSession?.orders[indexPath.section - 1] else {
                return cell
            }
            switch indexPath.row {
            case 0:
                let nameString = "\(String(order.quantity) ) \(order.item.name)"
                cell.itemNameLabel.text = nameString
                cell.priceLabel.text = "$\(String(format: "%.2f", order.basePrice))"
                return cell
            default:
                let nameString = "\(order.addOns[indexPath.row - 1].quantity) \(order.addOns[indexPath.row - 1].addOn.name)"
                cell.itemNameLabel.text = nameString
                cell.priceLabel.text = "$\(String(format: "%.2f", order.addOns[indexPath.row - 1].totalCost))"
                cell.itemNameIndent.constant = 30
                return cell
            }
        }
        return cell
    }
    
    @objc func tipButtonPressed(sender: UIButton) {
        mealSession?.tip = Double(sender.tag) / 100
        tableView.reloadData()
    }
}

extension ReceiptViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0: // Title
            return 60
        default: return UITableView.automaticDimension
        }
    }
}
