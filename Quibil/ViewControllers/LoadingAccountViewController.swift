//
//  LoadingAccountViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/9/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit
import Firebase

class LoadingAccountViewController: UIViewController {

    var isLoading = false {
        willSet {
            if newValue != isLoading {
                if isLoading { showLoader() }
                else { removeLoader() }
            }
        }
    }
    
    // listener object for user
    var userListener: UInt?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isLoading = true
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (timer) in
            // check if user is signed in
            if Auth.auth().currentUser?.isAnonymous ?? true {
                let signUp = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
                self.navigationController?.pushViewController(signUp, animated: true)
                return
            }
            
            // get BrainTree token for user
            print("Getting Client Token ")
            APP.app.getClientToken(onSuccess: nil)
            
            // load user if not loaded
            if APP.app.currentUser == nil {
                let user = User(dic: [
                    "id" : Auth.auth().currentUser?.uid ?? "",
                    "name" : "newName",
                    "email" : "newEmail",
                    "phonenumber" : "newPhonenumber",
                    "profilePictureURL" : "newProfilePictureURL.absoluteString"
                    ] as [String:Any])
                APP.app.currentUser = user
            }
            APP.app.loadFromFirebase(uid: Auth.auth().currentUser!.uid, onCompletion: { error in
                if let err = error {
                    self.showError(title: "Error", message: err, buttonText: "Close", completion: nil)
                    return
                }
                // check for existing session
                if APP.app.currentSession != nil {
                    APP.app.currentSession?.place.loadPlace(onSuccess: { (placeError) in
                        if let err = placeError {
                             self.showError(title: "Error", message: err, buttonText: "Close", completion: nil)
                            return
                        }
                        print("Session \(APP.app.currentSession?.id ?? "NoSession") exists/")
                        let menu = WelcomeToViewController(nibName: "WelcomeToViewController", bundle: nil)
                        menu.datasource = self
                        self.navigationController?.pushViewController(menu, animated: true)
                    })
                } else {
                    // got to scanner
                    let qrCheckin = ScanCheckInQRCodeViewController(nibName: "ScanCheckInQRCodeViewController", bundle: nil)
                    self.navigationController?.pushViewController(qrCheckin, animated: true)
                }
            })
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

protocol MealSesssionDataSource {
    func mealSession(for: Any) -> MealSession?
}

extension LoadingAccountViewController: MealSesssionDataSource {
    func mealSession(for: Any) -> MealSession? {
        return APP.app.currentSession
    }
}
