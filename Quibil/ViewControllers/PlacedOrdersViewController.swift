//
//  PlacedOrdersViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 8/1/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

class PlacedOrdersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var orders: [MealOrder]? {
        get {
            return APP.app.currentSession?.orders
        }
    }
    var listner: UInt?
    
    var isLoading: Bool = false {
        didSet {
            if oldValue != isLoading {
                if isLoading {
                    self.showLoader()
                } else {
                    self.removeLoader()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UINib(nibName: "ItemPriceTableTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemPriceTableTableViewCell")
        refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        listner = APP.app.currentSession?.listenToOrders(onTrigger: {
            self.refresh()
            self.isLoading = false
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let lis = listner {
            APP.app.currentSession?.FIRref?.child("orders").removeObserver(withHandle: lis)
        }
    }
    
    func refresh() {
        tableView.reloadData()
    }
    
    @objc func deleteItem(sender: UIButton) {
        guard let indexPath = tableView.indexPathForSelectedRow else {
            // No row Selected
            print("No row selected")
            return
        }
        
        if indexPath.row == 0 {
            self.isLoading = true
            APP.app.currentSession?.orders.remove(at: indexPath.section - 1)
            APP.app.updateCurrentSessionOrders { (error) in
                if let err = error {
                    print(err)
                    return
                }
            }
        } else {
            self.isLoading = true
            APP.app.currentSession?.orders[indexPath.section - 1].addOns.remove(at: indexPath.row - 1)
            APP.app.updateCurrentSessionOrders { (error) in
                if let err = error {
                    print(err)
                    return
                }
            }
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.defaultDismiss()
    }
    
    @IBAction func payBillPressed(_ sender: Any) {
        let receipt = ReceiptViewController(nibName: "ReceiptViewController", bundle: nil)
        receipt.mealSession = APP.app.currentSession
        self.navigationController?.pushViewController(receipt, animated: true)
    }
    
    deinit {
        if let lis = listner {
            APP.app.currentSession?.FIRref?.child("orders").removeObserver(withHandle: lis)
        }
    }
    
}

extension PlacedOrdersViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return (orders?.count ?? 0) + 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case numberOfSections(in: tableView) - 1:
            return 1
        default:
            return (orders?[section - 1].addOns.count ?? 0) + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemPriceTableTableViewCell", for: indexPath) as! ItemPriceTableTableViewCell
        cell.seperatorView.isHidden = true
        cell.deleteImage.isHidden = false
        cell.deleteButton.isHidden = false
        cell.itemNameIndent.constant = 10
        switch indexPath.section {
        case 0 : // orders
            cell.itemNameLabel.text = "Orders:"
            cell.priceLabel.text = ""
            cell.deleteImage.isHidden = true
            cell.deleteButton.isHidden = true
            cell.itemNameIndent.constant = 10
            return cell
        case numberOfSections(in: tableView) - 1: // Total
            cell.itemNameLabel.text = "Subtotal:"
            cell.priceLabel.text = "$\(String(format: "%.2f", APP.app.currentSession?.subTotal  ?? 0))"
            cell.seperatorView.isHidden = false
            cell.deleteImage.isHidden = true
            cell.deleteButton.isHidden = true
            return cell
        default:
            break
        }
        let order = orders?[indexPath.section - 1]
        switch indexPath.row {
        case 0:
            let nameString = "\(String(order?.quantity ?? 0) ) \(order?.item.name ?? "ErrorWithEntry")"
            cell.itemNameLabel.text = nameString
            cell.deleteButton.addTarget(self, action: #selector(deleteItem), for: .touchUpInside)
            cell.priceLabel.text = "$\(String(format: "%.2f", order?.basePrice ?? 0))"
            return cell
        default:
            let nameString = "\(order?.addOns[indexPath.row - 1].quantity ?? 0) \(order?.addOns[indexPath.row - 1].addOn.name ?? "ErrorWithEntry")"
            cell.itemNameLabel.text = nameString
            cell.deleteButton.addTarget(self, action: #selector(deleteItem), for: .touchUpInside)
            cell.priceLabel.text = "$\(String(format: "%.2f", order?.addOns[indexPath.row - 1].totalCost ?? 0))"
            cell.itemNameIndent.constant = 30
            return cell
        }
    }
}

extension PlacedOrdersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == numberOfSections(in: tableView) - 1 {
            return 50
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
        case 0, (numberOfSections(in: tableView) - 1):
            return false
        default:
            return true
        }
    }
}
