//
//  ReceiptsViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 9/5/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

class ReceiptsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        return refreshControl
    }()
    
    var receipts: [MealSession] = []
    var tracker = SectionsTracker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        tableView.addSubview(refreshControl)
        tableView.register(UINib(nibName: "ItemPriceTableTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemPriceTableTableViewCell")
        backButton.setImage(#imageLiteral(resourceName: "backArrow").withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.addTarget(self, action: #selector(dismissPressed), for: .touchUpInside)
        refreshControl.beginRefreshingManually()
        self.loadData()
    }
    
    @objc func loadData() {
        APP.app.getPastSessions { (mealSessions, error) in
            if let err = error {
                print(err)
                self.refreshControl.endRefreshing()
                self.showError(title: "Error", message: err, buttonText: "Close", completion: nil)
                return
            }
            guard var sessions = mealSessions else {
                self.refreshControl.endRefreshing()
                self.showError(title: "Error", message: "Could not load recept history.", buttonText: "Close", completion: nil)
                return
            }
            sessions.sort(by: { (first, second) in
                (first.closedOutAt ?? Date(timeIntervalSince1970: 0)) >= (second.closedOutAt ?? Date(timeIntervalSince1970: 0))
            })
            
            // track dates strings for sections
            guard var currentDate = sessions.first?.closedOutAt else {
                // no receipts
                self.refreshControl.endRefreshing()
                self.showError(title: "No History", message: "No Receipt History", buttonText: "Close", completion: nil)
                self.tableView.reloadData()
                return
            }
            let tracker = SectionsTracker()
            var rows = 0
            var startIndex = 0
            for i in 0..<sessions.count {
                sessions[i].place.loadPlace(onSuccess: { (error) in
                    if let err = error {
                        print("\(sessions[i].id) could not load place because of \(err)")
                        return
                    }
                    self.tableView.reloadData()
                })
                // get date
                guard let date = sessions[i].closedOutAt else {
                    // if no date go to next receipt
                    rows += 1
                    continue
                }
                // check if date changed
                if date.toWrittenFormat() != currentDate.toWrittenFormat() {
                    // date changed, record rows for section in tracker
                    let section = SectionTrackerSection(title: currentDate.toWrittenFormat(), startIndex: startIndex, rows: rows)
                    tracker.sections.append(section)
                    // start new section
                    startIndex = i
                    rows = 1
                    currentDate = date
                } else {
                    // increment rows
                    rows += 1
                }
            }
            
            // do last section if any rows left
            if rows > 0 {
                let section = SectionTrackerSection(title: currentDate.toWrittenFormat(), startIndex: startIndex, rows: rows)
                tracker.sections.append(section)
            }
            self.receipts = sessions
            self.tracker = tracker
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func dismissPressed() {
        self.defaultDismiss()
    }
    
}

extension ReceiptsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return tracker.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracker.sections[section].rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemPriceTableTableViewCell", for: indexPath) as! ItemPriceTableTableViewCell
        cell.leadingBoxWidth.constant = -10
        guard let index = tracker.index(section: indexPath.section, row: indexPath.row) else {
            cell.itemNameLabel.text = "Loading"
            cell.priceLabel.text = ""
            return cell
        }
        let session = receipts[index]
        cell.itemNameLabel.text = session.place.name
        cell.priceLabel.text = "$\(String(format: "%.2f", session.total))"
        return cell
    }
}

extension ReceiptsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let title = tracker.sections[section].title else {
            return nil
        }
        let view = UIView()
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        view.addSubview(backgroundView)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        backgroundView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        backgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        let titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 14)
        titleLabel.textColor = UIColor.black
        view.addSubview(titleLabel)
        titleLabel.text = "\(title)"
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tracker.sections[section].title != nil {
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let index = tracker.index(section: indexPath.section, row: indexPath.row) else {
            self.showError(title: "Error", message: "Could not retrieve receipt.", buttonText: "Close", completion: nil)
            return
        }
        let receiptVC = ReceiptViewController()
        receiptVC.mealSession = receipts[index]
        receiptVC.payButtonHeight = 0
        self.navigationController?.pushViewController(receiptVC, animated: true)
    }
}
