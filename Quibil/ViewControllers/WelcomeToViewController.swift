//
//  WelcomeToViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/19/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit
import FirebaseFirestore

class WelcomeToViewController: UIViewController {
    
    @IBOutlet weak var placeTitle: UILabel!
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var backToMenuButton: UIButton!
    @IBOutlet weak var cashOutButton: UIButton!
    @IBOutlet weak var thankYouLable: UILabel!
    
    var datasource: MealSesssionDataSource?
    
    var isLoading: Bool = false {
        didSet {
            if oldValue != isLoading {
                if isLoading {
                    self.showLoader()
                } else {
                    self.removeLoader()
                }
            }
        }
    }
    
    var session: MealSession? {
        get {
            return datasource?.mealSession(for: self)
        }
    }
    
    var place: Place? {
        get {
            return session?.place
        }
    }
    
    var sessionStateListener: ListenerRegistration?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placeTitle.text = place?.name
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { (time) in
            self.backToMenuButton.isHidden = false
            self.cashOutButton.isHidden = false
        }
        Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { (timer) in
            let menu = MenuViewController(nibName: "MenuViewController", bundle: nil)
            menu.datasource = self.datasource
            self.navigationController?.pushViewController(menu, animated: true)
        }
        sessionStateListener = APP.app.currentUser?.FIRref.addSnapshotListener({ (snap, error) in
            if snap?.data()?["currentSessionState"] as? String == "CHECKED_OUT" || snap?.data()?["currentSessionState"] as? String == nil {
                APP.app.currentSession = nil
                self.navigationController?.popToRootViewController(animated: true)
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let cashoutButtonTitle = (session?.orders.count ?? 0) > 0 ? "CASH OUT" : "EXIT"
        placeTitle.text = (session?.orders.count ?? 0) > 0 ? "" : session?.place.name
        thankYouLable.text = (session?.orders.count ?? 0) > 0 ? "\nThank you for the order!\n\nIt will be out shortly!" : ""
        cashOutButton.setTitle(cashoutButtonTitle, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func menuPressed(_ sender: Any) {
        let menu = MenuViewController(nibName: "MenuViewController", bundle: nil)
        menu.datasource = self.datasource
        self.navigationController?.pushViewController(menu, animated: true)
    }
    
    @IBAction func cashoutPressed(_ sender: Any) {
        if (session?.orders.count ?? 0) > 0 {
            let receipt = ReceiptViewController(nibName: "ReceiptViewController", bundle: nil)
            receipt.mealSession = APP.app.currentSession
            self.navigationController?.pushViewController(receipt, animated: true)
            return
        } else {
            self.isLoading = true
            APP.app.cancelSession { (error) in
                self.isLoading = false
                if let err = error {
                    print(err)
                    self.showError(title: "Error", message: "Unable to cancel session." , buttonText: "Close", completion: nil)
                    return
                }
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    deinit {
        sessionStateListener?.remove()
    }
}
