//
//  ScanCheckInQRCodeViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/16/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit
import AVFoundation

class ScanCheckInQRCodeViewController: UIViewController {

    @IBOutlet weak var titleBackgroundView: UIView!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var manualCaptureButton: UIButton!
    @IBOutlet weak var toggleFlashLight: UIButton!
    @IBOutlet weak var flashIconImage: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var confirmView: UIView!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var isLoading: Bool = false {
        didSet {
            if oldValue != isLoading {
                if isLoading {
                    self.showLoader()
                } else {
                    self.removeLoader()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toggleFlashLight.addTarget(self, action: #selector(toggleFlash), for: .touchUpInside)
        previewView.backgroundColor = UIColor.black
        setUpVideoCapture()
        
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = titleBackgroundView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        titleBackgroundView.addSubview(blurEffectView)
        
        let topBlurEffect = UIBlurEffect(style: .dark)
        let topBlurEffectView = UIVisualEffectView(effect: topBlurEffect)
        topBlurEffectView.frame = topView.bounds
        topBlurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        topView.addSubview(topBlurEffectView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setUpVideoCapture() {
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        do { videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice) }
        catch { return }
        
        if (captureSession.canAddInput(videoInput)) { captureSession.addInput(videoInput) }
        else { failed(); return; }
        
        let metadataOutput = AVCaptureMetadataOutput()
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else { failed(); return; }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = previewView.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        previewView.layer.addSublayer(previewLayer)
        confirmView.layer.borderColor = UIColor(red: 47/255, green: 197/255, blue: 54/255, alpha: 1).cgColor
        confirmView.layer.cornerRadius = confirmView.frame.height / 2
        confirmView.clipsToBounds = true
        captureSession.startRunning()
        
        switch videoCaptureDevice.torchMode {
        case .auto: flashIconImage.image = #imageLiteral(resourceName: "flashAuto")
        case .on: flashIconImage.image = #imageLiteral(resourceName: "flashOn")
        case .off: flashIconImage.image = #imageLiteral(resourceName: "flashOff")
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // set layer to view
        previewLayer.frame = previewView.layer.bounds
    }
    
    func failed() {
        // present error popup
        let ac = UIAlertController(title: "Scanning not supported",
                                   message: "Your device does not support scanning a code from an item. Please use a device with a camera.",
                                   preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.setNeedsStatusBarAppearanceUpdate()
        if (captureSession?.isRunning == false) { self.previewLayer.session?.startRunning() }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // stop video capture
        if (captureSession?.isRunning == true) { captureSession.stopRunning() }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Retreive QR Code
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            
            // Deal with prefix
            var code = stringValue
            if stringValue.uppercased().hasPrefix("QUIBIL:") {
                code = String(stringValue.dropFirst("QUIBIL:".count))
            } else if stringValue.uppercased().hasPrefix("QUIBIL") {
                code = String(stringValue.dropFirst("QUIBIL".count))
            }
            
            // Process QR Code
            found(code: code)
            
            // Update Capture Session
            confirmView.layer.borderWidth = 5
            self.previewLayer.session?.stopRunning()
        }
    }
    
    func found(code: String) {
        // Present QR Code
        let checkinVC = CheckIntoPlaceViewController()
        checkinVC.qrcode = code
        checkinVC.modalPresentationStyle = .overCurrentContext
        checkinVC.checkinBlock = { self.navigationController?.popToRootViewController(animated: true) }
        checkinVC.cancelBlock = {
            self.previewLayer.session?.startRunning()
            self.confirmView.layer.borderWidth = 0
        }
        self.present(checkinVC, animated: true, completion: nil)
    }
    
    @IBAction func receiptsPressed(_ sender: Any) {
//        let receiptsVC = ReceiptsViewController()
//        receiptsVC.receipts = []
//        self.navigationController?.pushViewController(receiptsVC, animated: true)
        let profileView = ProfileView()
        self.navigationController?.pushViewController(profileView, animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    
    @IBAction func capture(_ sender: Any) {
        previewLayer.session?.startRunning()
        self.confirmView.layer.borderWidth = 0
    }
    
    @objc func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else {
            self.showError(title: "Error", message: "Could not find camera.", buttonText: "Close", completion: nil)
            return
        }
        
        // check for flash
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                if (device.torchMode == AVCaptureDevice.TorchMode.on) { device.torchMode = AVCaptureDevice.TorchMode.off }
                else { do { try device.setTorchModeOn(level: 1.0) } catch { print(error) } }
                device.unlockForConfiguration()
                switch device.torchMode {
                case .auto:
                    flashIconImage.image = #imageLiteral(resourceName: "flashAuto")
                case .on:
                    flashIconImage.image = #imageLiteral(resourceName: "flashOn")
                case .off:
                    flashIconImage.image = #imageLiteral(resourceName: "flashOff")
                }
            } catch {
                print(error)
            }
        }
    }

}

extension ScanCheckInQRCodeViewController: AVCaptureMetadataOutputObjectsDelegate {
}
