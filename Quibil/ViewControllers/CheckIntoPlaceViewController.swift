//
//  CheckIntoPlaceViewController.swift
//  Quibil
//
//  Created by Michael Villanueva on 9/11/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit
import Braintree
import BraintreeDropIn

class CheckIntoPlaceViewController: UIViewController {
    
    @IBOutlet weak var squareView: UIView!
    @IBOutlet weak var messageTitleLabel: UILabel!
    @IBOutlet weak var checkInButton: UIButton!
    @IBOutlet weak var placeLogoImageView: UIImageView!
    @IBOutlet weak var viewBoxWidthRatio: NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var qrcode: String?
    var place: Place?
    var tableId: String?
    
    var cancelBlock: (() -> Void)?
    var checkinBlock: (() -> Void)?
    
    var isLoading: Bool = false {
        didSet {
            if oldValue != isLoading {
                if isLoading {
                    messageTitleLabel.isHidden = true
                    checkInButton.isHidden = true
                    placeLogoImageView.isHidden = true
                    activityIndicator.startAnimating()
                } else {
                    messageTitleLabel.isHidden = false
                    checkInButton.isHidden = false
                    placeLogoImageView.isHidden = false
                    activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isLoading = true
        processQRCode { (error) in
            if let err = error {
                print(err)
                self.showError(title: "Error", message: err, buttonText: "Close", completion: nil)
            }
            self.loadPlace()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func processQRCode(_ callback: ((_ error:String?) -> Void)? ) {
        APP.app.getRestaurantId(qrCode: qrcode ?? "error") { (restId, tableId, error) in
            if let err = error {
                callback?(err)
                return
            }
            guard let placeId = restId else {
                callback?("Could not retrieve restaurant")
                return
            }
            guard let tableId = tableId else {
                callback?("Could not retrieve table")
                return
            }
            self.place = Place(id: placeId, name: "Loading")
            self.tableId = tableId
            callback?(nil)
        }
    }
    
    private func loadPlace() {
        activityIndicator.startAnimating()
        place?.loadPlace(onSuccess: { (placeError) in
            if let err = placeError {
                print(err)
                self.dismiss(animated: true, completion: nil)
                return
            }
            self.viewBoxWidthRatio = self.viewBoxWidthRatio.setMultiplier(multiplier: 8/10)
            self.messageTitleLabel.text = self.place?.name
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            }) { (success) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.isLoading = false
                })
            }
            self.place?.loadlogoImage(onCompletion: { (logoError) in
                self.activityIndicator.stopAnimating()
                if let err = logoError {
                    print(err)
                    self.placeLogoImageView.image = self.place?.logoImage ?? #imageLiteral(resourceName: "quibilLogo")
                    return
                }
                self.placeLogoImageView.image = self.place?.logoImage ?? #imageLiteral(resourceName: "quibilLogo")
            })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String, onCompletion callback: ((String?, BTPaymentMethodNonce?) -> Void)? ) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
                self.dismiss(animated: true) {
                    callback?(error?.localizedDescription, nil)
                }
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
                self.dismiss(animated: true) {
                    callback?("CANCELLED", nil)
                }
            } else if let result = result {
                guard let method = result.paymentMethod else {
                    return
                }
                controller.dismiss(animated: true, completion: {
                    callback?(nil, method)
                })
            }
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
    
    @IBAction func checkin(_ sender: Any) {
        isLoading = true
        guard let place = place else {
            print("No place loaded")
            return
        }
        guard let tableId = tableId else {
            print("No tableId loaded")
            return
        }
        guard let tokenKey = APP.app.braintreeClientToken else {
            print("No client token was generated")
            return
        }
        
        // Get payment info
        showDropIn(clientTokenOrTokenizationKey: tokenKey) { error,payment in
            if let err = error {
                // check if they cancelled
                if err == "CANCELLED" {
                    let alert = UIAlertController(title: nil, message: "You must save payment details before checking into a restaurant.", preferredStyle: .alert)
                    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                        alert.dismiss(animated: false)
                        Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { (timer) in
                            self.cancel(self)
                        })
                    })
                    let givePayment = UIAlertAction(title: "Save Payment Details", style: .default, handler: { (action) in
                        alert.dismiss(animated: true)
                        Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { (timer) in
                            self.checkin(self)
                        })
                    })
                    alert.addAction(cancel)
                    alert.addAction(givePayment)
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                print(err)
                self.showError(title: "Error", message: err, buttonText: "Close", completion: nil)
                self.isLoading = false
                return
            }
            
            guard let paymentnonce = payment else {
                print("Error: No Payment Method")
                self.showError(title: "Error", message: "Could not process payment method", buttonText: "Close", completion: nil)
                return
            }
            APP.app.currentPayment = paymentnonce
            
            // Check into place
            APP.app.checkInto(restaurantId: place.id, tableId: "", onCompletion: { (results, error) in
                // create session
                guard let orderSessionId = results?["sessionId"] as? String else {
                    print("No Session")
                    self.showError(title: "Error", message: "Could not check in. \(String(describing: error))", buttonText: "Close", completion: nil)
                    return
                }
                APP.app.currentSession = MealSession(id: orderSessionId, place: place, tableId: tableId)
                APP.app.state = .checkedin
                self.checkinBlock?()
                self.defaultDismiss()
            })
            
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        isLoading = false
        cancelBlock?()
        self.defaultDismiss()
    }
    
}
