//
//  SignUpViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/9/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import Photos

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var profilePictureSubtitle: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var addPictureButton: UIButton!
    @IBOutlet weak var verificationMessageLabel: UILabel!
    @IBOutlet weak var sendCodeButton: UIButton!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var verificationLoading: UIActivityIndicatorView!
    @IBOutlet weak var newButton: UIButton!
    
    var isLoading = false {
        willSet {
            if newValue != isLoading {
                if isLoading { showLoader() }
                else { removeLoader() }
            }
        }
    }
    
    enum SignUpState {
        case fillingOutInformation, sendVerificationCode, verificationCodeSent, verificationComplete, verificationError, verificationIncorrect
    }
    
    var signUpState: SignUpState = .fillingOutInformation {
        didSet {
            switch signUpState {
            case .fillingOutInformation:
                verificationMessageLabel.text = "After you fill out your information, please verify your phonenumber by sending a code and confirming it."
            case .sendVerificationCode:
                verificationMessageLabel.text = "Please confirm phonenumber with verification code."
            case .verificationComplete:
                verificationMessageLabel.text = "Verification Successfull!"
            default: break
            }
        }
    }
    
    var profilePic: UIImage?
    var email: String?
    var name: String?
    var phonenumber: String?
    var profilePictureURL: URL?
    
    var picker = UIImagePickerController()
    
    var isFilledOUt: Bool {
        get {
            return profilePic != nil && email != nil && name != nil && phonenumber != nil && profilePictureURL != nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (Auth.auth().currentUser?.isAnonymous ?? false) == true {
            Auth.auth().signInAnonymously(completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        profilePicture.layer.borderWidth = 1.0
        profilePicture.layer.borderColor = UIColor.black.cgColor
        profilePicture.clipsToBounds = true
        picker.delegate = self
        picker.allowsEditing = true
        nameTextField.delegate = self
        phoneTextField.delegate = self
        emailTextField.delegate = self
        codeTextField.delegate = self
        addPictureButton.addTarget(self, action: #selector(addProfilePicture), for: .touchUpInside)
        viewDidLayoutSubviews()
        sendCodeButton.addTarget(self, action: #selector(requestVerifcationCode), for: .touchUpInside)
        newButton.addTarget(self, action: #selector(signUpUser), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profilePicture.layer.cornerRadius = profilePicture.frame.height / 2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func addProfilePicture(sender: UIButton) {
        let menu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhoto = UIAlertAction(title: "Take a Photo", style: .default) { (action) in
            self.picker.sourceType = .camera
            self.picker.cameraCaptureMode = .photo
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                self.present(self.picker, animated: true, completion: nil)
            } else {
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                    if granted {
                        self.present(self.picker, animated: true, completion: nil)
                    } else {
                        self.showError(title: "Error",
                                       message: "Please grant camera permissions to Quibil to take a photo.",
                                       buttonText: "Close",
                                       completion: nil )
                    }
                })
                self.refresh()
            }
        }
        let fromLibrary = UIAlertAction(title: "Choose from Library", style: .default) { (action) in
            self.picker.sourceType = .photoLibrary
            if PHPhotoLibrary.authorizationStatus() ==  .authorized {
                self.present(self.picker, animated: true, completion: nil)
            } else {
                PHPhotoLibrary.requestAuthorization({ (newStatus) in
                    if (newStatus == PHAuthorizationStatus.authorized) {
                        self.present(self.picker, animated: true, completion: nil)
                    } else {
                        self.showError(title: "Error",
                                       message: "Could not open photo library. Please check permissions.",
                                       buttonText: "Close",
                                       completion: nil)
                    }
                    self.refresh()
                })
            }
        }
        let cancel = UIAlertAction(title: "Cancel",
                                   style: .cancel) { (action) in
            menu.dismiss(animated: true, completion: nil)
        }
        
        menu.addAction(takePhoto)
        menu.addAction(fromLibrary)
        menu.addAction(cancel)
        
        self.present(menu, animated: true, completion: nil)
    }
    
    func refresh() {
        DispatchQueue.main.async {
            self.profilePicture.image = self.profilePic != nil ? self.profilePic : #imageLiteral(resourceName: "cameraIcon")
            self.profilePicture.contentMode = self.profilePic != nil ? .scaleAspectFit : .center
        }
    }
    
    @objc func requestVerifcationCode() {
        guard let num = phonenumber else {
            self.showError(title: "Error", message: "Please provide a phonenumber", buttonText: "Close", completion: nil)
            return
        }
        APP.requestVerificationCode(phonenumber: num) { (response, error) in
            if let err = error {
                print(err)
                self.showError(title: "Error", message: err.localizedDescription, buttonText: "Close", completion: nil)
                return
            }  
            guard let res = response else {
                self.showError(title: "Error", message: "Unable to complete request.", buttonText: "Close", completion: nil)
                return
            }
            self.signUpState = .verificationCodeSent
            self.showError(title: "Code Sent", message: res["message"] as? String, buttonText: "Close", completion: nil)
        }
    }
    
    @objc func signUpUser() {
        guard let newEmail = email else {
            return
        }
        guard let newName = name else {
            return
        }
        guard let newPhonenumber = phonenumber else {
            return
        }
        guard let newProfilePictureURL = profilePictureURL else {
            return
        }
        let credential = EmailAuthProvider.credential(withEmail: newEmail, password: newPhonenumber)
        Auth.auth().currentUser?.linkAndRetrieveData(with: credential, completion: { (signInResult, error1) in
            guard let id = Auth.auth().currentUser?.uid else {
                print("Error: Unsuccessful")
                return
            }
            guard let user = User(dic: [
                "id" : id,
                "name" : newName,
                "email" : newEmail,
                "phoneNumber" : newPhonenumber,
                "profilePictureURL" : newProfilePictureURL.absoluteString
                ]) else {
                    return
            }
            APP.app.currentUser = user
            print("Sign Up Successfull")
            APP.app.updateUser(onCompletion: { (response, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                print("User in database")
                self.showError(title: "Sign Up Complete", message: "You have made an account", buttonText: "Close", completion: nil)
                self.navigationController?.popToRootViewController(animated: true)
            })
        })
    }
    
    @IBAction func LoginButtonPressed(_ sender: Any) {
        let login = LoginViewController()
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case emailTextField: email = emailTextField.text
        case nameTextField: name = nameTextField.text
        case phoneTextField: phonenumber = phoneTextField.text
        default: break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == codeTextField {
            let code = codeTextField.text ?? ""
            verificationLoading.startAnimating()
            APP.verify(verificationCode: code, phonenumber: phonenumber ?? "") { (response , error) in
                self.verificationLoading.stopAnimating()
                if let err = error {
                    print(err)
                    self.showError(title: "Error", message: "Could not verify code", buttonText: "Close", completion: nil)
                    return
                }
                guard let res = response else {
                    return
                }
                self.showError(title: "Success", message: res["message"] as? String, buttonText: "Close", completion: nil)
                return
            }
        }
        
        textField.resignFirstResponder()
        return true
    }
}

extension SignUpViewController: UINavigationControllerDelegate {}

extension SignUpViewController: UIImagePickerControllerDelegate {
    
    // Did not select/take a picture
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // get image
        guard let image = info[.editedImage] as? UIImage else {
            // dismiss picker and display error
            picker.dismiss(animated: true, completion: {
                self.showError(title: "Error",
                               message: "Could not retrieve image. Try again.",
                               buttonText: "Close",
                               completion: nil)
            })
            return
        }
        
        // upload image
        picker.showLoader()
        print("Uploading Photo")
        APP.upload(profilePicture: image) { (url, error) in
            picker.removeLoader()
            if let err = error {
                picker.dismiss(animated: true, completion: {
                    self.refresh()
                    self.showError(title: "Error", message: err.localizedDescription, buttonText: "Close", completion: nil)
                    print(err)
                    print(err.localizedDescription)
                })
            } else {
                picker.dismiss(animated: true, completion: {
                    self.profilePic = image
                    self.profilePictureURL = url
                })
            }
        }
    }
}
