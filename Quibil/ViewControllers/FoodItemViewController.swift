//
//  FoodItemViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/31/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

class FoodItemViewController: UIViewController {

    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var backArrowImage: UIImageView!
    @IBOutlet weak var itemTableView: UITableView!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var addToOrderButton: UIButton!
    @IBOutlet weak var quantityLabel: UILabel!
    
    var item: Item?
    var quantity = 1
    var addOns = [MealOrderItemAddOn]()
    
    var isLoading: Bool = false {
        didSet {
            if oldValue != isLoading {
                if isLoading {
                    self.showLoader()
                } else {
                    self.removeLoader()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backArrowImage.image = #imageLiteral(resourceName: "backArrow").withRenderingMode(.alwaysTemplate)
        backArrowImage.tintColor = UIColor.black
        itemNameLabel.text = item?.name
        itemPriceLabel.text = "$\( String(format: "%.2f", item?.price ?? 0) )"
        
        itemTableView.dataSource = self
        itemTableView.delegate = self
        itemTableView.register(UINib(nibName: "FoodItemAddOnTableViewCell", bundle: nil), forCellReuseIdentifier: "FoodItemAddOnTableViewCell")
        itemTableView.tableFooterView = UIView()
        itemTableView.tableFooterView?.backgroundColor = UIColor(white: 0.8, alpha: 1.0)
        
        addOns = item?.addOns?.map({ (addOn) -> MealOrderItemAddOn in
            return MealOrderItemAddOn(addOn: addOn, quantity: 0)
        }) ?? []
        
        addToOrderButton.addTarget(self, action: #selector(orderItem), for: .touchUpInside)
        refresh()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func refresh() {
        itemNameLabel.text = item?.name
        itemPriceLabel.text = "$\( String(format: "%.2f", item?.price ?? 0) )"
        quantityLabel.text = String(quantity)
        itemTableView.reloadSections([0], with: .none)
        
    }
    
    @objc func orderItem() {
        if item == nil {
            self.showError(title: "Error", message: "Could not retrieve food item", buttonText: "Close", completion: nil)
        }
        let order = MealOrder(item: item!, quantity: quantity)
        order.addOns = addOns.filter({ (addOn) -> Bool in
            return addOn.quantity > 0
        })
        isLoading = true
        APP.app.addOrderToSession(order: order) { (error) in
            print("Order has been added.")
            self.isLoading = false
            for vc in self.navigationController?.viewControllers ?? [] {
                if let welcome = vc as? WelcomeToViewController {
                    self.navigationController?.popToViewController(welcome, animated: true)
                    return
                }
            }
        }
    }
    
    @objc func addAddOn(sender: UIView) {
        addOns[sender.tag].quantity += 1
        itemTableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
    }
    
    @objc func minusAddOn(sender: UIView) {
        if addOns[sender.tag].quantity == 0 { return }
        addOns[sender.tag].quantity -= 1
        itemTableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.defaultDismiss()
    }
    
    @IBAction func addQuantity(_ sender: Any) {
        quantity += 1
        refresh()
    }
    @IBAction func minusQuantity(_ sender: Any) {
        if quantity > 1 {
            quantity -= 1
        }
        refresh()
    }
}

extension FoodItemViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        // return number of available fields
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: // addOns
            return addOns.count
        case 1:
            return 1
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0: // Add On
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodItemAddOnTableViewCell", for: indexPath) as! FoodItemAddOnTableViewCell
            cell.addon = addOns[indexPath.row]
            cell.quantityLabel.text = String(cell.addon?.quantity ?? 0)
            cell.itemNameLabel.text = item?.addOns?[indexPath.row].name
            cell.addButton.addTarget(self, action: #selector(addAddOn), for: .touchUpInside)
            cell.addButton.tag = indexPath.row
            cell.minusButton.addTarget(self, action: #selector(minusAddOn), for: .touchUpInside)
            cell.minusButton.tag = indexPath.row
            return cell
        case 1: // Request Order
            break
        default: break
        }
        let cell = UITableViewCell()
        return cell
    }
}

extension FoodItemViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
