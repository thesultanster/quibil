//
//  Menu.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/25/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation

class Menu {
    var categories = [FoodCategory]()
    init() {}
    
    convenience init?(dic: [String:Any]) {
        self.init()
        for categoryData in dic {
            if let categoryDic = categoryData.value as? [String:Any] {
                if let category = FoodCategory(dic: categoryDic) {
                    categories.append(category)
                }
            }
        }
    }
}
