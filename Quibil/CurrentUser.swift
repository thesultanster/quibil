//
//  CurrentUser.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 11/30/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Firebase

class CurrentUser {
    
    static var current = CurrentUser()
    
    func user(callback: ((User?, Error?) -> ())? ) {
        guard let id = Auth.auth().currentUser?.uid else {
            callback?(nil, CurrentUserError.NoUserLoggedIn)
            return
        }
        Firestore.firestore().document("/users/\(id)").getDocument { (userDocSnap, error) in
            guard let userDic = userDocSnap?.data() else {
                callback?(nil, CurrentUserError.CouldNotReadUserData)
                return
            }
            print(userDic)
            guard let user = User(dic: userDic) else {
                callback?(nil, CurrentUserError.CouldNotReadUserData)
                return
            }
            
            // Get Profile Picture
            guard let url = user.profilePictureURL else {
                callback?(user,CurrentUserError.CouldNotRetrieveProfilePicture)
                return
            }
            let ref = Storage.storage().reference(forURL: url.absoluteString)
            ref.getData(maxSize: IMAGE_LIMIT) { (data, error) in
                if let err = error {
                    print(err)
                    callback?(user,err)
                    return
                }
                guard let value = data else {
                    callback?(user,CurrentUserError.CouldNotRetrieveProfilePicture)
                    return
                }
                guard let image = UIImage(data: value) else {
                    callback?(user,CurrentUserError.CouldNotRetrieveProfilePicture)
                    return
                }
                user.profilePicture = image
                callback?(user,nil)
            }
            callback?(user, nil)
        }
        
        
    }
    
    func user(update: [String:Any], callback: ((Error?) -> ())? ) {
        guard let id = Auth.auth().currentUser?.uid else {
            callback?(CurrentUserError.NoUserLoggedIn)
            return
        }
        Firestore.firestore().document("users/\(id)").updateData(update) { (error) in
            callback?(error)
        }
    }
    
    func login(userEmail: String, password: String, callback: ((Error?) -> ())? ) {
        Auth.auth().signIn(withEmail: userEmail, password: password) { (authResult, error) in
            callback?(error)
        }
    }
}

enum CurrentUserError : Error {
    case NoUserLoggedIn, CouldNotReadUserData, CouldNotRetrieveProfilePicture
}
