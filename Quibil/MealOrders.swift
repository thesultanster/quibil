//
//  MealOrders.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 8/18/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation

class MealOrder {
    let item: Item
    let quantity: Int
    var state: State
    var addOns = [MealOrderItemAddOn]()
    var totalCost: Double {
        get {
            var cost = item.price
            for addOn in addOns {
                cost += addOn.totalCost
            }
            return cost * Double(quantity)
        }
    }
    
    var basePrice: Double {
        return item.price * Double(quantity)
    }
    
    enum State: String {
        case deliverd = "DELIVERED",
            ordered = "ORDERED"
    }
    
    var hash : [String:Any] {
        get {
            var addOnsHash = [[String:Any]]()
            for addOn in addOns {
                addOnsHash.append(addOn.hash)
            }
            return [
                "id" : item.id,
                "name" : item.name,
                "price" : Int(item.price * 100),
                "quantity": quantity,
                "totalCost" : Int(totalCost * 100),
                "addOns" : addOnsHash,
                "state" : state.rawValue
            ]
        }
    }
    
    init(item: Item, quantity: Int) {
        self.item = item
        self.quantity = quantity
        self.state = .ordered
    }
    
    convenience init(item: Item, quantity: Int, addOns: [MealOrderItemAddOn]) {
        self.init(item: item, quantity: quantity)
        self.addOns = addOns
    }
    
    convenience init?(dic: [String:Any]) {
        guard let id = dic["id"] as? String  else {
             return nil
        }
        guard let name = dic["name"] as? String else {
            return nil
        }
        guard let price = dic["price"] as? Double else {
            return nil
        }
        guard let quantity = dic["quantity"] as? Int else {
            return nil
        }
        let item = Item(id: id, name: name, price: price / 100, categoryId: "", restaurantId: "")
        self.init(item: item, quantity: quantity)
        if let addOnData = dic["addOns"] as? [[String:Any]] {
            for addOnDic in addOnData {
                guard let addOn = MealOrderItemAddOn(dic: addOnDic) else {
                    print("Error: Was unable to process order addon")
                    continue
                }
                addOns.append(addOn)
            }
        }
        if let state = dic["state"] as? State {
            self.state = state
        }
    }
}

class MealOrderItemAddOn {
    let addOn: FoodItemAddOn
    var quantity: Int = 0
    
    var totalCost: Double {
        get {
            return addOn.price * Double(quantity)
        }
    }
    
    var hash : [String:Any] {
        get {
            return [
                "id" : addOn.id,
                "name" : addOn.name,
                "price" : addOn.price * 100,
                "quantity" : quantity,
                "totalCost" : totalCost * 100
            ]
        }
    }
    
    init(addOn: FoodItemAddOn, quantity: Int) {
        self.addOn = addOn
        self.quantity = quantity
    }
    
    convenience init?(dic: [String:Any]) {
        guard let id = dic["id"] as? String else { return nil }
        
        guard let name = dic["name"] as? String else { return nil }
        
        guard let price = dic["price"] as? Double else { return nil }
        
        guard let quantity = dic["quantity"] as? Int else { return nil }
        
        let addOn = FoodItemAddOn(id: id, name: name, price: price / 100)
        self.init(addOn: addOn, quantity: quantity)
    }
}
