//
//  MealSession.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 8/15/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation
import Firebase

class MealSession {
    let id: String
    let place: Place
    let tableId: String
    var orders = [MealOrder]()
    var state: MealState
    var listener: UInt?
    var closedOutAt: Date?
    var createdAt: Date?
    var closedOutById: String?
    
    var FIRref: DatabaseReference? {
        get {
            return Database.database().reference(withPath: "order_sessions/\(id)")
        }
    }

    var subTotal: Double {
        get {
            var tab: Double = 0
            for order in orders {
                tab += order.totalCost
            }
            return tab
        }
    }
    var tax: Double = 0.0975
    var tip: Double = 0.15
    var totalTax: Double { get { return subTotal * tax } }
    var totalTip: Double {
        get {
            return (subTotal + tax) * tip
        }
    }
    
    var total: Double {
        get {
            return subTotal + totalTax + totalTip
        }
    }
    
    init(id: String, place: Place, tableId: String) {
        self.id = id
        self.place = place
        self.tableId = tableId
        self.state = .started
    }
    
    convenience init?(dic: [String:Any]) {
        
        // Make sure it has an id, placeId, and a tableId
        guard let id = dic["id"] as? String else {
            print("No id")
            return nil
        }
        guard let placeId = dic["restaurantId"] as? String else {
            print("No restaurant id")
            return nil
        }
        guard let tableId = dic["tableId"] as? String else {
            print("No table id")
            return nil
        }
        
        // Set Up place
        let place = Place(id: placeId, name: "Loading")
        place.name = dic["restaurantName"] as? String ?? "Loading"
        place.loadPlace(onSuccess: nil)
        
        // init
        self.init(id: id, place: place, tableId: tableId)
        
        // get orders
        if let ordersArray = dic["orders"] as? [[String:Any]] {
            orders = ordersArray.compactMap({ orderData in MealOrder(dic: orderData) })
            print("\(id) has \(orders.count) orders.")
        }
        
        // get other optional variables
        if let closeOutTimeStamp = dic["closedOutAt"] as? Int64 {
            self.closedOutAt = Date(timeIntervalSince1970: TimeInterval(closeOutTimeStamp / 1000))
        }
        if let createdAtTimeStamp = dic["createdAt"] as? Int64 {
            print(Double(createdAtTimeStamp / 1000))
            self.createdAt = Date(timeIntervalSince1970: TimeInterval(createdAtTimeStamp / 1000))
        }
        self.closedOutById = dic["closedOutById"] as? String
    }
    
    // MARK: - Enums
    enum ConstructionError: Error {
        case Missing_Id, Missing_Place, Missing_Table_Id
    }
    enum MealState: String {
        case started = "STARTED", ordering = "ORDERED", checkingout = "CHECKINGOUT", payed = "PAYED", check_out = "CHECKED_OUT"
    }
    
    func listenToOrders(onTrigger callback: (() -> Void)?) -> UInt {
        return (FIRref?.child("orders").observe(.value, with: { (snap) in
            // sync orders
            var orders = [MealOrder]()
            if let ordersData = snap.value as? [[String:Any]] {
                for orderDic in ordersData {
                    guard let order = MealOrder(dic: orderDic) else {
                        print("Error: Was Not able to process order")
                        continue
                    }
                    orders.append(order)
                }
                self.orders = orders
            }
            callback?()
        }))!
    }
    
    func stopListeners() { FIRref?.removeAllObservers() }
    
    deinit { stopListeners() }
}
