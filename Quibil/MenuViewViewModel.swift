//
//  MenuViewViewModel.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 9/30/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MenuViewViewModel {
    
    // MARK: Private
    private let _itemsDataSource: Variable<[Item]> = Variable([])
    private let _categoryDataSource: Variable<[String]> = Variable([])
    private let disposeBag = DisposeBag()
    
    // MARK: Outputs
    public let itemsDataSource: Observable<[Item]>
    public let categoryDataSource: Observable<[String]>
    
    init(menu: Menu) {
        self.itemsDataSource = _itemsDataSource.asObservable()
        self.categoryDataSource = _categoryDataSource.asObservable()
    }
    
}
