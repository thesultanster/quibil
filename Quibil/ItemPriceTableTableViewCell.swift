//
//  ItemPriceTableTableViewCell.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 8/18/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

class ItemPriceTableTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var itemNameIndent: NSLayoutConstraint!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var deleteImage: UIImageView!
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var buttonWidth: NSLayoutConstraint!
    @IBOutlet weak var leadingBoxWidth: NSLayoutConstraint!
    @IBOutlet weak var trailingIconWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        deleteImage.contentMode = .scaleAspectFit
        deleteImage.image = #imageLiteral(resourceName: "cancelIcon").withRenderingMode(.alwaysTemplate)
        boxView.layer.cornerRadius = 10
        boxView.layer.borderColor = UIColor(white: 0.2, alpha: 0.15).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.boxView.layer.borderWidth = selected ? 1 : 0
        self.buttonWidth.constant = selected ? 30 : 0
        self.leadingBoxWidth.constant = selected ? -5 : 0
        self.trailingIconWidth.constant = selected ? 5 : 5
        UIView.animate(withDuration: 0.3) {
            self.boxView.layer.borderColor = selected ? UIColor(white: 0, alpha: 0.15).cgColor : UIColor(white: 0, alpha: 0).cgColor
            self.contentView.layoutIfNeeded()
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.boxView.layer.borderWidth = highlighted ? 1 : 0
        self.buttonWidth.constant = highlighted ? 30 : 0
        self.leadingBoxWidth.constant = highlighted ? -5 : 0
        self.trailingIconWidth.constant = highlighted ? 5 : 5
        UIView.animate(withDuration: 0.3) {
            self.boxView.layer.borderColor = highlighted ? UIColor(white: 0, alpha: 0.15).cgColor : UIColor(white: 0, alpha: 0).cgColor
            self.contentView.layoutIfNeeded()
        }
    }
    
}
