//
//  FoodItemAddOnTableViewCell.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 8/1/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

class FoodItemAddOnTableViewCell: UITableViewCell {

    @IBOutlet weak var selectImage: UIImageView!
    @IBOutlet weak var selectItemButton: UIButton!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    
    @IBOutlet weak var quantityLabel: UILabel!
    var addon: MealOrderItemAddOn?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectImage.layer.borderColor = UIColor.black.cgColor
        selectImage.layer.borderWidth = 1
        selectImage.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        selectImage.layer.cornerRadius = selectImage.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
