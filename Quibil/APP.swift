//
//  APP.swift
//  
//
//  Created by Michael Ryan Santos Villanueva on 7/15/18.
//

import Foundation
import UIKit
import Firebase
import TwilioAuth
import Alamofire
import Braintree

// MARK: - Globals -

// Limit images to 5 mb
let IMAGE_LIMIT: Int64 = 5 * 1024 * 1024

enum LOGIN_ERROR : Error {
    case NO_USER_LOGGED_IN
}

extension LOGIN_ERROR : LocalizedError {
    var errorDescription : String? {
        switch self {
        case .NO_USER_LOGGED_IN :
            return NSLocalizedString("No user logged in.", comment: "User must be logged in for this action")
        }
    }
}

class APP {
    
    // Singleton for user and network calls
    static let app = APP()
    
    // Twilio
    static let twilioShared = TwilioAuth.sharedInstance()
    static let TWILIO_API_KEY = "mq25dhAdjzbKv9AStbeTLTLpZXiu5Cc7"
    
    // Brain Tree
    let BRAINTREE_KEY = "sandbox_sthw6p5j_tgzjdsnxds69y7xs"
    var braintreeClientToken: String? = nil
    
    // User
    var currentUser: User?
    var currentSession: MealSession?
    var state: AppState = .startup
    
    var currentPayment: BTPaymentMethodNonce?
    
    var firestore: Firestore {
        get {
            return Firestore.firestore()
        }
    }
    
    enum AppState: Int {
        case startup, loadingAccount, signup, checkingIn, checkedin, ordering, checkingOut, paying
    }
    
    enum ClientError: Error {
        case CouldNotConvertImageToFile, CouldNotUploadImageFile
    }
    
    enum ResponseError: Error {
        case InvalidURL
    }
    
    // Reference for Firebase Storage
    static var storageRef : StorageReference {
        get { return Storage.storage().reference() }
    }
    
    // return a nil if unsuccessfull with error string
    static func upload(profilePicture image: UIImage, onResponse callback: ((URL?, Error?) -> Void)? ) {
        guard let data = image.pngData() else {
            callback?(nil,ClientError.CouldNotConvertImageToFile)
            return
        }
        let metaData = StorageMetadata()
        metaData.contentType = "image/png"
        let ref = storageRef.child("/profilePictures/\(UUID().uuidString)")
        ref.putData(data, metadata: metaData) { (metadata, error) in
            if error != nil {
                callback?(nil,error)
                return
            } else {
                print("Image file was uploaded.")
                ref.downloadURL(completion: {
                    if $1 != nil {
                        callback?(nil,ClientError.CouldNotUploadImageFile)
                        return
                    }
                    guard let url = $0 else {
                        callback?(nil,ClientError.CouldNotUploadImageFile)
                        return
                    }
                    callback?(url,nil)
                    return
                })
            }
        }
    }
    
    func saveUserProfile(onCompletion callback: ((Error?) -> Void)?) {
        if Auth.auth().currentUser != nil && currentUser != nil {
            callback?(("No User Logged In" as! Error))
        }
    }
    
    func logout(callback: ((Error?) -> ())? ) {
        do {
            try Auth.auth().signOut()
            callback?(nil)
        } catch let err {
            callback?(err)
        }
    }
    
    static func loginUser(email: String, password: String, onCompletion callback: ((Error?) -> Void)?) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let err = error {
                callback?(err)
                return
            }
            guard let authResult = result else {
                callback?(("Could Not Confirm Sign In" as! Error))
                return
            }
            print("User \(authResult.user.uid) was signed in.")
            callback?(nil)
            return
        }
    }
    
    func updateCurrentSessionOrders(onCompletion callback: ((String?) -> Void)?) {
        guard let session = APP.app.currentSession else {
            callback?("No Sesssion")
            return
        }
        let orders = session.orders.compactMap { (order) in
            order.hash
        }
        session.FIRref?.updateChildValues(["orders" : orders], withCompletionBlock: { (error, ref) in
            if let err = error {
                callback?(err.localizedDescription)
                return
            }
            callback?(nil)
        })
    }
    
    func getRestaurantId(qrCode string: String, _ callback: ((_ restaurantId:String?, _ tableId: String?, _ error:String?)  -> ())? ) {
        firestore.document("qrcodes/\(string)").getDocument { snap, error in
            if let err = error {
                callback?(nil,nil,err.localizedDescription)
                return
            }
            guard let qrcodeEntry = snap?.data() else {
                print("No data for qrcode \"\(string)\" with path \(snap?.reference.path ?? "No Path")")
                callback?(nil,nil,"Could not process qrcode entry, Err: 0")
                return
            }
            guard let restaurantId = qrcodeEntry["restaurantId"] as? String else {
                callback?(nil,nil,"Could not process qrcode entry, Err: 1")
                return
            }
            guard let tableId = qrcodeEntry["tableId"] as? String else {
                callback?(nil,nil,"Could not process qrcode entry, Err: 2")
                return
            }
            
            callback?(restaurantId, tableId, nil)
        }
    }
    
    func getRestaurant(id: String, onCompletion callback: ((Place?,Error?) -> Void)?) {
        guard let url = URL(string: "https://us-central1-quibil-b5335.cloudfunctions.net/getRestaurant") else { return }
        let params = [
            "restaurantId" : id
        ] as [String:String]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
        .responseJSON { (response) in
            if let error = response.error {
                callback?(nil,error)
                return
            }
            guard let res = response.result.value as? [String:Any] else {
                callback?(nil,("Could not process results. Err: 0" as! Error))
                return
            }
            guard let placeData = res["restaurant"] as? [String:Any] else {
                callback?(nil,("Could not process results. Err: 1" as! Error))
                return
            }
            guard let restaurant = Place(dic: placeData) else {
                callback?(nil,("Could not process results. Err: 2" as! Error))
                return
            }
            callback?(restaurant,nil)
            return
        }
    }
    
    static func signUp(user: User, onCompletion callback: ((Error?) -> Void)?) {
    }
    
    func closeOutSession(paymentMethod: BTPaymentMethodNonce, onSucces callback: ((String?) -> Void)?) {
        guard let url = URL(string: "https://us-central1-quibil-b5335.cloudfunctions.net/clientCloseOut") else {
            callback?("Error with URL")
            return
        }
        guard let session = currentSession else {
            callback?("Error with Session")
            return
        }
        let body = [
            "sessionId" : session.id,
            "paymentMethodNonce" : paymentMethod.nonce,
            "amount" : session.subTotal
        ] as [String:Any]
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            callback?(nil)
        }
        
    }
    
    static func requestVerificationCode(phonenumber: String, onCompletion callback: (([String:Any]?,Error?) -> Void)?) {
        guard let url = URL(string: "https://api.authy.com/protected/json/phones/verification/start") else {
            return
        }
        let params = [
            "api_key" : TWILIO_API_KEY,
            "via" : "sms",
            "phone_number" : phonenumber,
            "country_code" : "1"
            
        ] as [String:String]
        let header: HTTPHeaders = [
            "X-Authy-API-Key": TWILIO_API_KEY
        ]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            print("Response from Twilio Verification request.")
            if let error = response.error {
                print(error)
                callback?(nil,error)
                return
            }
            print(response.debugDescription)
            
            guard let res = response.result.value as? [String:Any] else {
                print("Could not process results.")
                callback?(nil,nil)
                return
            }
            callback?(res,nil)
            return
        }
        
    }
    
    func getPastSessions(onComplete callback: ((_ sessions: [MealSession]?, _ error: String?) -> Void)?) {
        guard let user = currentUser else {
            callback?(nil,"No User Logged In.")
            return
        }
        
        let receiptCollectionRef = firestore.collection("clientSessionHistory/\(user.id)/closedOutSessions")
        receiptCollectionRef.limit(to: 50).order(by: "closedOutAt", descending: true).getDocuments { snap, error in
            if let err = error {
                callback?(nil, err.localizedDescription)
            }
            guard let receiptSnapshots = snap?.documents else {
                callback?(nil, "Could not retrieve receipt snapshots.")
                return
            }
            print(receiptSnapshots.count)
            let receipts = receiptSnapshots.compactMap({ (receiptSnap) -> MealSession? in
                return MealSession(dic: receiptSnap.data())
            })
            print(receipts.count)
            callback?(receipts,nil)
        }
    }
    
    // returns error message string if any
    func getClientToken(onSuccess callback: ((String?) -> Void)?) {
        print("getting client token")
        // get params
        guard let clientId = Auth.auth().currentUser?.uid else {
            callback?("No user logged in.")
            return
        }
        guard let url = URL(string: "https://us-central1-quibil-b5335.cloudfunctions.net/generateBTClientToken") else {
            callback?("Could not generate request.")
            return
        }
        let body = [ "clientId" : clientId ] as [String:Any]
        
        // make request
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: nil)
        .responseJSON { (response) in
            print("***                                 ***")
            print(response)
            print("***                                 ***")
            if let value = response.value as? [String:Any] {
                print(value)
                // set client token
                guard let clientToken = value["clientToken"] as? String else {
                    callback?("No Client Token Recieved")
                    return
                }
                self.braintreeClientToken = clientToken
                print("client token was generated")
                callback?(nil)
                return
            } else {
                // No response
                callback?("502 Gateway Timeout: No Response from server.")
            }
        }
    }
    
    static func verify(verificationCode: String, phonenumber: String, onCompletion callback: (([String:Any]?,Error?) -> Void)?) {
        guard let url = URL(string: "https://api.authy.com/protected/json/phones/verification/check?phone_number=\(phonenumber)&country_code=1&verification_code=\(verificationCode)") else {
            return
        }
        let header: HTTPHeaders = [
            "X-Authy-API-Key": TWILIO_API_KEY
        ]
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            print("Response from Twilio Verification Check Request.")
            if let error = response.error {
                print(error)
                callback?(nil,error)
                return
            }
            print(response.debugDescription)
            
            guard let res = response.result.value as? [String:Any] else {
                print("Could not process results.")
                callback?(nil,nil)
                return
            }
            callback?(res,nil)
            return
        }
        
    }
    
    func loadFromFirebase(uid: String, onCompletion callback: ((String?) -> Void)?) {
        Firestore.firestore().document("/users/\(uid)").getDocument { (userDocSnap, error) in
            if let err = error {
                callback?(err.localizedDescription)
                return
            }
            guard let dic = userDocSnap?.data() else {
                callback?("Could not get user from data.")
                return
            }
            
            guard let user = User(dic: dic) else {
                callback?("Could not retrieve user from data.")
                return
            }
            self.currentUser = user
            
            if let sessionId = dic["currentSession"] as? String {
                Database.database().reference(withPath: "order_sessions/\(sessionId)").observeSingleEvent(of: .value, with: { (snap) in
                    if !snap.hasChildren() {
                        callback?(nil)
                        return
                    }
                    guard let sessionDic = snap.value as? [String:Any] else {
                        callback?(nil)
                        return
                    }
                    guard let session = MealSession(dic: sessionDic) else {
                        callback?(nil)
                        return
                    }
                    APP.app.currentSession = session
                    session.place.loadPlace(onSuccess: { err in
                        if let erro = err {
                            callback?(erro)
                            session.place.loadPlace(onSuccess: nil)
                            return
                        } else {
                            callback?(nil)
                            return
                        }
                    })
                })
            } else {
                callback?(nil)
            }
        }
    }
    
    func cancelSession(onCompletion callback: ((String?) -> Void)?) {
        guard let session = currentSession else {
            callback?("No Current Session")
            return
        }
        guard let user = currentUser else {
            callback?("No Current User")
            return
        }
        if session.orders.count > 0 {
            callback?("Session has active orders")
            return
        }
        var errorString = ""
        var removedFromCheckedIn = false
        var removedFromUser = false
        Database.database().reference().child("restaurant_sessions/\(session.place.id)/checkedIn/\(session.id)").removeValue { (error, ref) in
            errorString += error?.localizedDescription ?? ""
            removedFromCheckedIn = true
            if removedFromUser && removedFromCheckedIn {
                if errorString != "" { callback?(errorString) }
                else {
                    self.currentSession = nil
                    callback?(nil)
                }
            }
        }
        Firestore.firestore().document("/users/\(user.id)").updateData([
            "currentSession" : FieldValue.delete(),
            "currentSessionState" : FieldValue.delete()
        ]) { (error) in
            errorString += error?.localizedDescription ?? ""
            removedFromUser = true
            if removedFromUser && removedFromCheckedIn {
                if errorString != "" { callback?(errorString) }
                else {
                    self.currentSession = nil
                    callback?(nil)
                }
            }
        }
        
    }
    
    func updateUser(onCompletion callback: (([String:Any]?,Error?) -> Void)?) {
        
        guard let url = URL(string: "https://us-central1-quibil-b5335.cloudfunctions.net/updateUser") else {
            return
        }
        
        guard let user = currentUser else {
            return
        }
        let params = [
            "id" : user.id,
            "name" : user.name,
            "firstName" : user.name,
            "lastName" : "",
            "phoneNumber" : user.phonenumber,
            "profilePictureURL" : user.profilePictureURL?.absoluteString ?? "",
            "email" : user.email
        ] as [String:Any]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            print("Response from Twilio Verification Check Request.")
            if let error = response.error {
                print(error)
                callback?(nil,error)
                return
            }
            print(response.debugDescription)
            
            guard let res = response.result.value as? [String:Any] else {
                print("Could not process results.")
                callback?(nil,nil)
                return
            }
            callback?(res,nil)
            return
        }
    }
    
    // MARK: - Session Functions -
    
    func checkInto(restaurantId: String, tableId: String, onCompletion callback: (([String:Any]?,Error?) -> Void)?) {
        
        guard let url = URL(string: "https://us-central1-quibil-b5335.cloudfunctions.net/checkinUserToRestaurant") else {
            return
        }
        guard let user = currentUser else {
            print("No User Logged In")
            return
        }
        let params = [
            "clientId" : user.id,
            "restaurantId" : restaurantId,
            "tableId" : tableId
        ] as [String:Any]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            print("Response from Twilio Verification Check Request.")
            if let error = response.error {
                print(error)
                callback?(nil,error)
                return
            }
            print(response.debugDescription)
            
            guard let res = response.result.value as? [String:Any] else {
                print("Could not process results.")
                callback?(nil,nil)
                return
            }
            callback?(res,nil)
            return
        }
    }
    
    func addOrderToSession(order: MealOrder, onSuccess callback: ((String?) -> Void)?) {
        print("Adding order to session")
        currentSession?.FIRref?.child("state").setValue("PENDING_FOOD_ORDER")
        currentSession?.FIRref?.child("orders").runTransactionBlock({ (currentData) -> TransactionResult in
            print(currentData)
            if var ordersData = currentData.value as? [Any] {
                print(ordersData)
                ordersData.append(order.hash)
                currentData.value = ordersData
                return TransactionResult.success(withValue: currentData)
            } else {
                currentData.value = [order.hash]
                return TransactionResult.success(withValue: currentData)
            }
        }, andCompletionBlock: {(error, committed, snapshot) in
            if let error = error {
                print(error.localizedDescription)
                callback?(error.localizedDescription)
            } else {
                print("Order was written.")
                callback?(nil)
            }
        })
    }
    
}

extension String {
    func remove(prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}

extension Date {
    
    // print Date in with format of Jan 2, 1999
    func toWrittenFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: self)
    }
    
}

extension UIView {
    
    // overlay loading view on top of view
    func showLoader(){
        let loadingView = UIView()
        loadingView.backgroundColor = UIColor(white: 0.0, alpha: 0.2)
        loadingView.frame = self.bounds
        loadingView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        loadingView.tag = 1534234523
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.startAnimating()
        loadingView.addSubview(activityIndicator)
        activityIndicator.center = loadingView.center
        self.addSubview(loadingView)
    }
    
    // remove loading overlay from view
    func removeLoader(){
        self.subviews.compactMap {  $0 as UIView }.forEach {
            if $0.tag == 1534234523 { $0.removeFromSuperview() }
        }
    }
}

extension NSLayoutConstraint {
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem as Any,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

extension UIViewController {
    
    var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func showLoader() { self.view.showLoader() }

    func removeLoader() { self.view.removeLoader() }
    
    func showError(title: String?, message: String?, buttonText: String?, completion: (()->Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let button = UIAlertAction(title: buttonText, style: .cancel) { (action) in
            completion?()
            alert.dismiss(animated: true, completion: completion)
        }
        alert.addAction(button)
        self.present(alert, animated: true, completion: nil)
    }
    
    func defaultDismiss() {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension UIRefreshControl {
    func beginRefreshingManually() {
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: true)
        }
        beginRefreshing()
    }
}

