//
//  Place.swift
//  
//
//  Created by Michael Ryan Santos Villanueva on 7/25/18.
//

import Foundation
import UIKit
import Firebase
import Alamofire

class Place {
    let id: String
    var name: String
    var phonenumber: String?
    var address: String?
    var menu: Menu?
    
    var bannerImageURL: String?
    var logoImageURL: String?
    
    var bannerImage: UIImage?
    var logoImage: UIImage?
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    convenience init?(dic: [String:Any]) {
        guard let id = dic["id"] as? String else {
            print("No Id")
            return nil
        }
        guard let name = dic["name"] as? String else {
            print("No Name")
            self.init(id: id, name: "Error")
            return
        }
        self.init(id: id, name: name)
        
        if let menuDic = dic["menu"] as? [String:Any] {
            if let menu = Menu(dic: menuDic) { self.menu = menu }
            else { print("No Menu") }
        }
        self.logoImageURL = dic["profilePictureUrl"] as? String
        self.phonenumber = dic["phonenumber"] as? String
        self.address = dic["address"] as? String
    }
    
    func loadBanner(onCompletion callback: ((String?) -> Void)?) {
        guard let url = bannerImageURL else {
            callback?("No banner image url.")
            return
        }
        Storage.storage().reference(forURL: url).getData(maxSize: IMAGE_LIMIT) { (data, error) in
            if let err = error {
                callback?(err.localizedDescription)
            }
            guard let value = data else {
                callback?("Could not read data")
                return
            }
            guard let image = UIImage(data: value) else {
                callback?("Could not create image")
                return
            }
            self.bannerImage = image
        }
    }
    
    /**
     Load logo image for restaurant with completion block
     */
    func loadlogoImage(onCompletion callback: ((String?) -> Void)?) {
        guard let url = logoImageURL else {
            callback?("No logo image url")
            return
        }
        Storage.storage().reference(forURL: url).getData(maxSize: IMAGE_LIMIT) { (data, error) in
            if let err = error {
                callback?(err.localizedDescription)
            }
            guard let value = data else {
                callback?("Could not read data")
                return
            }
            guard let image = UIImage(data: value) else {
                callback?("Could not create image")
                return
            }
            self.logoImage = image
            callback?(nil)
        }
    }
    
    /**
     Load place information from database
     
     - parameters:
        completion block with error string if any, nil otherwise.
    */
    func loadPlace(onSuccess callback: ((String?) -> Void)?) {
        guard let url = URL(string: "https://us-central1-quibil-b5335.cloudfunctions.net/getRestaurant") else {
            return
        }
        
        let params = [
            "restaurantId" : id
        ] as [String:String]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if let error = response.error {
                callback?(error.localizedDescription)
                return
            }
            guard let res = response.result.value as? [String:Any] else {
                callback?("Could not read place data.")
                return
            }
            guard let placeData = res["restaurant"] as? [String:Any] else {
                callback?("Could not process place data")
                return
            }
            guard let name = placeData["name"] as? String else {
                callback?("No Name")
                return
            }
            self.name = name
            if let menuDic = placeData["menu"] as? [String:Any] {
                if let menu = Menu(dic: menuDic) { self.menu = menu }
                else { print("No Menu") }
            }
            self.logoImageURL = placeData["profilePictureUrl"] as? String
            self.phonenumber = placeData["phonenumber"] as? String
            self.address = placeData["address"] as? String
            callback?(nil)
            return
        }
    }
}
