//
//  Colors.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 9/21/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation
import UIKit

class Colors {
    
    static let primaryColor = UIColor(red: 54/255, green: 182/255, blue: 85/255, alpha: 1.0)
    static let unselected = UIColor(white: 0.8, alpha: 1)
}
