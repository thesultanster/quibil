//
//  FoodCategory.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/25/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation

class FoodCategory {
    let id: String
    let name: String
    var items = [Item]()
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    convenience init?(dic: [String:Any]) {
        guard let id = dic["id"] as? String else {
            print("No Id for category")
            return nil
        }
        guard let name = dic["name"] as? String else {
            print("No name for category")
            return nil
        }
        self.init(id: id, name: name)
        
        if let itemsDic = dic["items"] as? [String:Any] {
            for itemData in itemsDic.values {
                if let itemDic = itemData as? [String:Any] {
                    if let item = Item(dic: itemDic) {
                        items.append(item)
                    }
                }
            }
        }
    }
}
