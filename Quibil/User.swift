//
//  User.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/16/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation
import UIKit
import os.log
import Firebase

class User {
    
    var id: String
    var name: String
    var email: String
    var phonenumber: String
    var profilePicture: UIImage?
    var profilePictureURL: URL?
    var receipts: [MealSession] = []
    var braintreeId: String?
    
    var FIRref: DocumentReference {
        get {
            return Firestore.firestore().document("users/\(id)")
        }
    }
    
    var hash: [String:Any] {
        get {
            return [
                "id" : id,
                "name" : name,
                "email" : email,
                "phoneNumber" : phonenumber,
                "profilePictureURL" : profilePictureURL?.absoluteString ?? ""
            ]
        }
    }
    
    init() {
        id = ""
        name = ""
        email = ""
        phonenumber = ""
        profilePictureURL = nil
    }
    
    convenience init?(dic: [String:Any]) {
        guard let id = dic["id"] as? String else {
            return nil
        }
        guard let name = dic["name"] as? String else {
            return nil
        }
        guard let email = dic["email"] as? String else {
            return nil
        }
        guard let phonenumber = dic["phoneNumber"] as? String else {
            return nil
        }
        guard let profilePictureURL = URL(string: (dic["profilePictureURL"] as? String ?? "")) else {
            return nil
        }
        self.init()
        self.id = id
        self.name = name
        self.email = email
        self.phonenumber = phonenumber
        self.profilePictureURL = profilePictureURL
    }
    
    // get the user object from firebase
    func loadFromFirebase(onCompletion callback: ((String?) -> Void)?) {
        Firestore.firestore().document("/users/\(self.id)").getDocument { (userDocSnap, error) in
            
            if let err = error { callback?(err.localizedDescription); return; }
            guard let dic = userDocSnap?.data() else {
                callback?("Could not get user from data.")
                return
            }
            
            if let id = dic["id"] as? String { self.id = id }
            if let name = dic["name"] as? String { self.name = name }
            if let email = dic["email"] as? String { self.email = email }
            if let phonenumber = dic["phonenumber"] as? String { self.phonenumber = phonenumber }
            if let profilePictureURL = URL(string: (dic["profilePictureURL"] as? String ?? "")) {
                self.profilePictureURL = profilePictureURL
            }
            
            callback?(nil)
        }
    }
    
    
    // get Profile Picture from FireStorage
    func loadProfilePicture(onCompletion callback: ((Error?) -> Void)?) {
        guard let url = profilePictureURL else {
            callback?(("Invalide Profile Picture URL Reference." as! Error))
            return
        }
        let ref = Storage.storage().reference(forURL: url.absoluteString)
        
        // Get Profile Picture with limit of 5mb
        ref.getData(maxSize: IMAGE_LIMIT) { (data, error) in
            if let err = error {
                print(err)
                callback?(error)
                return
            }
            guard let value = data else {
                callback?(("Could not process image data." as! Error))
                return
            }
            guard let image = UIImage(data: value) else {
                callback?(("Could not convert data to an image." as! Error))
                return
            }
            self.profilePicture = image
            callback?(nil)
        }
    }
}
