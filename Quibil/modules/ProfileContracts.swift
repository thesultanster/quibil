//
//  ProfileContracts.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

//MARK: View -
/*
 Should replace "class" with "BaseViewProtocol" if available;
 & that will allow the View to act as a UIViewController;
 & Implement common view functions.
 */
/// Profile Module View Protocol
protocol ProfileViewProtocol: class {
    // Update UI with value returned.
    /// Set the view Object of Type ProfileEntity
    func set(object: ProfileEntity)
}

//MARK: Interactor -
/// Profile Module Interactor Protocol
protocol ProfileInteractorProtocol {
    // Fetch Object from Data Layer
    func fetch(objectFor presenter: ProfilePresenterProtocol)
}

//MARK: Presenter -
/// Profile Module Presenter Protocol
protocol ProfilePresenterProtocol {
    /// The presenter will fetch data from the Interactor thru implementing the Interactor fetch function.
    func fetch(objectFor view: ProfileViewProtocol)
    /// The Interactor will inform the Presenter a successful fetch.
    func interactor(_ interactor: ProfileInteractorProtocol, didFetch object: ProfileEntity)
    /// The Interactor will inform the Presenter a failed fetch.
    func interactor(_ interactor: ProfileInteractorProtocol, didFailWith error: Error)
}

//MARK: Router (aka: Wireframe) -
/// Profile Module Router Protocol
protocol ProfileRouterProtocol {
    // Show Details of Entity Object coming from ParentView Controller.
    // func showDetailsFor(object: ProfileEntity, parentViewController viewController: UIViewController)
}
