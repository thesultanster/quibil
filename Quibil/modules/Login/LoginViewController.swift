//
//  LoginViewController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 12/4/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var email: String?
    var password: String?
    
    var isLoading: Bool = false {
        didSet {
            if oldValue != isLoading {
                if isLoading {
                    self.showLoader()
                } else {
                    self.removeLoader()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // set delegate
        emailTextField.delegate = self
        passwordTextField.delegate = self
        compensateForKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barStyle = .default
    }
    
    func login() {
        guard let email = email else {
            self.showError(title: "Error", message: "Please provide an email.", buttonText: "Close", completion: nil)
            return
        }
        guard let password = password else {
            self.showError(title: "Error", message: "Please provide a password.", buttonText: "Close", completion: nil)
            return
        }
        isLoading = true
        CurrentUser.current.login(userEmail: email, password: password) { [unowned self] (error) in
            self.isLoading = false
            if let err = error {
                print(err.localizedDescription)
                self.showError(title: "Error", message: err.localizedDescription, buttonText: "Close", completion: nil)
                return
            }
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        login()
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // keyboard stuff
    
    func compensateForKeyboard(){
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillChange),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillChange(notification: NSNotification) {
        print("Keyboard was presented")
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.1 , animations: {
                self.bottomConstraint.constant = keyboardSize.height
                self.view.layoutSubviews()
                print(self.bottomConstraint.constant)
            })
        } else { print("Could not get Keyboard Size") }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1 , animations: {
            self.bottomConstraint.constant = 0
            self.view.layoutSubviews()
            print(self.bottomConstraint.constant)
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension LoginViewController : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case emailTextField:
            email = emailTextField.text
        case passwordTextField:
            password = passwordTextField.text
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField: // go to password
            passwordTextField.becomeFirstResponder()
        case passwordTextField: // attempt login
            passwordTextField.resignFirstResponder()
            // call login
            login()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
}
