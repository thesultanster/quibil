//
//  ProfilePresenter.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

/// Profile Module Presenter
class ProfilePresenter {
    weak private var _view: ProfileViewProtocol?
    private var view: ProfileViewProtocol? { get { return _view } }
    private var interactor: ProfileInteractorProtocol
    private var wireframe: ProfileRouterProtocol
    
    init(view: ProfileViewProtocol) {
        self._view = view
        self.interactor = ProfileInteractor()
        self.wireframe = ProfileRouter()
        self.interactor.fetch(objectFor: self)
    }
}

extension ProfilePresenter: ProfilePresenterProtocol {
    func fetch(objectFor view: ProfileViewProtocol) {
        print("Asked to fetch from Profile Object from interactor")
    }
}

// Callbacks from the interactor
extension ProfilePresenter: ProfileInteractorToPresenter {
    func interactor(_ interactor: ProfileInteractorProtocol, didFetch object: ProfileEntity) {
        print("Was able to fetch profile from Interactor")
        view?.set(object: object, isLoading: false)
    }
    
    func interactor(_ interactor: ProfileInteractorProtocol, didLogout message: String) {
        print("Interactor did logout with message")
        print("Logout Message to Presenter: \(message)")
        view?.logout()
    }
    
    func interactor(_ interactor: ProfileInteractorProtocol, didFailWith error: Error) {
        print("Interactor fails with error")
        // Tell View to display error
        view?.display(errorMessage: error.localizedDescription)
    }
}

// User Input Functions
extension ProfilePresenter : ProfileViewToPresenter {
    func profileView(_ profileView: ProfileView, newName: String) {
        print("View told  Presenter there is a new name")
        // interactor to update name
    }
    
    func profileView(_ profileView: ProfileView, newEmail: String) {
        print("View told Presenter there is a new email")
        // interactor to update name
    }
    
    func profileView(_ profileView: ProfileView, newPhoneNumber: String) {
        print("View told Presenter there is a new phoneNumber")
        // interactor to update phone number
    }
    
    func profileView(_ profileView: ProfileView, didSelectLogout: Bool) {
        print("View told presenter logout was clicked")
        interactor.logout(logoutFor: self)
    }
    
    func profileView(_ profileView: ProfileView, didSelectDismiss: Bool) {
        print("view told presenter dismiss was clicked")
        print("dismissing view")
        profileView.defaultDismiss()
    }
    
    func profileView(_ profileView: ProfileView, didSelectReceipts: Bool) {
        print("View told presenter reciepts was clicked.")
        // tell router to display reciepts module
        wireframe.generateReceiptsModule(for: self)
    }
}

// Generated External Modules
extension ProfilePresenter: ProfileRouterToPresenterProtocol {
    func router(didGenerate view: UIViewController) {
        self.view?.push(view: view)
    }
}
