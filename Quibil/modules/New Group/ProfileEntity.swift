//
//  ProfileEntity.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

/// Profile Module Entity
struct ProfileEntity {
    var id: String
    var name: String
    var email: String
    var phonenumber: String
    var profilePicture: UIImage?
    var profilePictureURL: URL?
    
    init(_ id: String, _ name: String, _ email: String, _ phonenumber: String) {
        self.id = id
        self.name =  name
        self.email = email
        self.phonenumber = phonenumber
    }
}
