//
//  ProfileView.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

/// Profile Module View
class ProfileView: UIViewController {
    
    private let ui = ProfileViewUI()
    private var _presenter: ProfilePresenterProtocol!
    private var presenter: ProfileViewToPresenter {
        get {
            return _presenter
        }
    }
    
    private var object : ProfileEntity?
    
    override func loadView() {
        // setting the custom view as the view controller's view
        ui.delegate = self
        ui.dataSource = self
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _presenter = ProfilePresenter(view: self)
        
        // Informs the Presenter that the View is ready to receive data.
        presenter.fetch(objectFor: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ui.reloadData()
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.setNeedsStatusBarAppearanceUpdate()
    }
}

// MARK: - extending ProfileView to implement it's protocol
extension ProfileView: ProfileViewProtocol {
    func push(view: UIViewController) {
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func set(object: ProfileEntity, isLoading: Bool) {
        print("setting up view with profile")
        self.object = object
        ui.reloadData()
        ui.isLoading = isLoading
    }
    
    func logout() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func display(errorMessage: String) {
        self.showError(title: "Error", message: errorMessage, buttonText: "Close", completion: nil)
    }
}

// MARK: - extending ProfileView to implement the custom ui view delegate
extension ProfileView: ProfileViewUIDelegate {
    
    func didEnterNewName(profile: ProfileViewUI) {
        print("UI tells view user put in a new name")
        presenter.profileView(self, newName: ui.nameTextField.text ?? "")
    }
    
    func didEnterNewEmail(profile: ProfileViewUI) {
        print("UI tells view user put in a new email")
        presenter.profileView(self, newEmail: ui.nameTextField.text ?? "")
    }
    
    func didEnterNewPhoneNumber(profile: ProfileViewUI) {
        print("UI tells view user put in a new phonenumber")
        presenter.profileView(self, newPhoneNumber: ui.phonenumberTextField.text ?? "")
    }
    
    func didPressLogoutFor(profile: ProfileViewUI) {
        print("UI tells view user pressed logout")
        presenter.profileView(self, didSelectLogout: true)
    }
    
    func didPressDismiss(for profile: ProfileViewUI) {
        print("UI tells view user pressed dismiss")
        presenter.profileView(self, didSelectDismiss: true)
    }
    
    func didPressReceipts(for profile: ProfileViewUI) {
        print("UI tells view user pressed receipts")
        presenter.profileView(self, didSelectReceipts: true)
    }
}

// MARK: - extending ProfileView to implement the custom ui view data source
extension ProfileView: ProfileViewUIDataSource {
    // Pass the pre-defined object to the dataSource.
    func objectFor(ui: ProfileViewUI) -> ProfileEntity {
        return object ?? ProfileEntity("", "", "", "")
    }
}
