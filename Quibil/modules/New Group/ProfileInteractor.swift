//
//  ProfileInteractor.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

/// Profile Module Interactor
class ProfileInteractor: ProfileInteractorProtocol {
    func fetch(objectFor presenter: ProfilePresenterProtocol) {
        CurrentUser.current.user { (user, error) in
            if let error = error {
                 print(error)
                presenter.interactor(self, didFailWith: error)
            }
            guard let user = user else { return }
            var profile = ProfileEntity(user.id, user.name, user.email, user.phonenumber)
            profile.profilePicture = user.profilePicture
            // get Profile Picture
            presenter.interactor(self, didFetch: profile)
        }
    }
    
    func update(update : [String: Any], for presenter: ProfilePresenterProtocol) {
        CurrentUser.current.user(update: update) { (error) in
            if let err = error {
                presenter.interactor(self, didFailWith: err)
                return
            }
        }
    }
    
    func logout(logoutFor presenter: ProfilePresenterProtocol) {
        APP.app.logout { [unowned self] (error) in
            if let error = error {
                presenter.interactor(self, didFailWith: error)
            } else {
                presenter.interactor(self, didLogout: "You were able to logout.")
            }
        }
    }
}
