//
//  ProfileRouter.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

/// Profile Module Router (aka: Wireframe)
class ProfileRouter: ProfileRouterProtocol {
    func showDetailsFor(object: ProfileEntity, parentViewController viewController: UIViewController) {
        if let nav = viewController.navigationController {
            print("pushing profile view to navigation")
            let vc = UIViewController()
            nav.pushViewController(vc, animated: true)
        } else {
            let vc = UIViewController()
            viewController.present(vc, animated: true)
        }
    }
    
    func generateReceiptsModule(for presenter: ProfileRouterToPresenterProtocol) {
        // Use Windows in the future
        let receipts = ReceiptsViewController()
        presenter.router(didGenerate: receipts)
    }
    
    func logout(profileView: UIViewController) {
        profileView.navigationController?.popToRootViewController(animated: true)
    }
}
