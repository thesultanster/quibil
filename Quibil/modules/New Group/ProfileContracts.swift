//
//  ProfileContracts.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

// MARK: View -
/*
 Should replace "class" with "BaseViewProtocol" if available;
 & that will allow the View to act as a UIViewController;
 & Implement common view functions.
 */
/// Profile Module View Protocol
protocol ProfileViewProtocol: class {
    // Update UI with value returned.
    /// Set the view Object of Type ProfileEntity
    func set(object: ProfileEntity, isLoading: Bool)
    func display(errorMessage: String)
    func logout()
    func push(view: UIViewController)
}

// MARK: - Interactor -
protocol ProfileInteractorProtocol {
    // Fetch Object from Data Layer
    func fetch(objectFor presenter: ProfilePresenterProtocol)
    func logout(logoutFor presenter: ProfilePresenterProtocol)
}

// MARK: - Presenter -
protocol ProfilePresenterProtocol: ProfileViewToPresenter, ProfileInteractorToPresenter, ProfileRouterToPresenterProtocol {
}

protocol ProfileInteractorToPresenter { // Interactor callbacks
    func interactor(_ interactor: ProfileInteractorProtocol, didFetch object: ProfileEntity)
    func interactor(_ interactor: ProfileInteractorProtocol, didLogout message: String)
    func interactor(_ interactor: ProfileInteractorProtocol, didFailWith error: Error)
}

protocol ProfileViewToPresenter { // View callbacks
    // View is ready to display object
    func fetch(objectFor view: ProfileViewProtocol)
    
    // View User Inputs
    func profileView(_ profileView: ProfileView, didSelectLogout: Bool)
    func profileView(_ profileView: ProfileView, didSelectDismiss: Bool)
    func profileView(_ profileView: ProfileView, didSelectReceipts: Bool)
    func profileView(_ profileView: ProfileView, newName: String)
    func profileView(_ profileView: ProfileView, newEmail: String)
    func profileView(_ profileView: ProfileView, newPhoneNumber: String)
}

protocol ProfileRouterToPresenterProtocol { // Router Callbacks
    // Another Module was genereated to present
    func router(didGenerate view: UIViewController)
}

//MARK: Router (aka: Wireframe) -
protocol ProfileRouterProtocol {
    // Show Details of Entity Object coming from ParentView Controller.
    func generateReceiptsModule(for presenter: ProfileRouterToPresenterProtocol)
}


