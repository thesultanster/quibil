//
//  ProfileViewUI.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

// MARK: ProfileViewUI Delegate -
/// ProfileViewUI Delegate
protocol ProfileViewUIDelegate {
    func didPressLogoutFor(profile: ProfileViewUI)
    func didPressReceipts(for profile: ProfileViewUI)
    func didPressDismiss(for profile: ProfileViewUI)
    func didEnterNewName(profile: ProfileViewUI)
    func didEnterNewEmail(profile: ProfileViewUI)
    func didEnterNewPhoneNumber(profile: ProfileViewUI)
}

// MARK: ProfileViewUI Data Source -
/// ProfileViewUI Data Source
protocol ProfileViewUIDataSource {
    // This will be implemented in the Module View.
    /// Set Object for the UI Component
    func objectFor(ui: ProfileViewUI) -> ProfileEntity
}

class ProfileViewUI: UIView {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phonenumberTextField: UITextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var receiptsButton: UIButton!
    @IBOutlet weak var receiptsIcon: UIImageView!
    
    var delegate: ProfileViewUIDelegate?
    var dataSource: ProfileViewUIDataSource?
    
    var object : ProfileEntity?
    var contentView = UIView()
    
    var isLoading: Bool = false {
        didSet {
            if oldValue != isLoading {
                if isLoading {
                    self.showLoader()
                } else {
                    self.removeLoader()
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUIElements()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUIElements()
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        setupConstraints()
        self.layoutIfNeeded()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        contentView.frame = self.bounds
        profileImageView.layer.cornerRadius = profileImageView.bounds.height / 2
    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        contentView = Bundle.main.loadNibNamed("ProfileViewUI", owner: self, options: nil)?.first as! UIView
        self.addSubview(contentView)
        nameTextField.delegate = self
        emailTextField.delegate = self
        phonenumberTextField.delegate = self
        receiptsIcon.image = receiptsIcon.image?.withRenderingMode(.alwaysTemplate)
    }
    
    fileprivate func setupConstraints() {
        // add constraints to subviews
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction private func logoutButtonPressed(_ sender: Any) {
        delegate?.didPressLogoutFor(profile: self)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        delegate?.didPressDismiss(for: self)
    }
    
    @IBAction func receiptsPressed(_ sender: Any) {
        delegate?.didPressReceipts(for: self)
    }
    
    /// Reloading the data and update the ui according to the new data
    func reloadData() {
        self.object = dataSource?.objectFor(ui: self)
        // update UI
        nameTextField.text = self.object?.name
        emailTextField.text = self.object?.email
        phonenumberTextField.text = self.object?.phonenumber
        profileImageView.image = self.object?.profilePicture ?? UIImage(imageLiteralResourceName: "quibilLogo")
    }
}

extension ProfileViewUI : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            nameTextField.resignFirstResponder()
        case emailTextField:
            emailTextField.resignFirstResponder()
        case phonenumberTextField:
            phonenumberTextField.resignFirstResponder()
        default: break
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case nameTextField:
            delegate?.didEnterNewEmail(profile: self)
        case emailTextField:
            delegate?.didEnterNewEmail(profile: self)
        case phonenumberTextField:
            delegate?.didEnterNewPhoneNumber(profile: self)
        default:
            break
        }
    }
}
