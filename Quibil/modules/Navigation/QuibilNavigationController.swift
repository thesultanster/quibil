//
//  QuibilNavigationController.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 12/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

class QuibilNavigationController: UINavigationController {
    
    var style: UIStatusBarStyle = .lightContent
    
    override var prefersStatusBarHidden: Bool {
         return false
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return style
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func changeStatusBar(to style: UIStatusBarStyle) {
        self.style = style
    }
    
}

class NavigationController: UINavigationController {
    
    var style: UIStatusBarStyle = .lightContent
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
//    @objc override var preferredStatusBarStyle: UIStatusBarStyle {
//        // return style
//        return style
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func changeStatusBar(to style: UIStatusBarStyle) {
        self.style = style
    }
    
}
