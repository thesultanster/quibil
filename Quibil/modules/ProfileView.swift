//
//  ProfileView.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

/// Profile Module View
class ProfileView: UIViewController {
    
    private let ui = ProfileViewUI()
    private var presenter: ProfilePresenterProtocol!
    
    private var object : ProfileEntity?
    
    override func loadView() {
        // setting the custom view as the view controller's view
        ui.delegate = self
        ui.dataSource = self
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ProfilePresenter(view: self)
        
        // Informs the Presenter that the View is ready to receive data.
        presenter.fetch(objectFor: self)
    }
    
}

// MARK: - extending ProfileView to implement it's protocol
extension ProfileView: ProfileViewProtocol {
    
}

// MARK: - extending ProfileView to implement the custom ui view delegate
extension ProfileView: ProfileViewUIDelegate {
    
}

// MARK: - extending ProfileView to implement the custom ui view data source
extension ProfileView: ProfileViewUIDataSource {
    // Pass the pre-defined object to the dataSource.
}
