//
//  ProfilePresenter.swift
//  Quibil
//
//  Created Michael Villanueva on 11/6/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import UIKit

/// Profile Module Presenter
class ProfilePresenter {
    
    weak private var _view: ProfileViewProtocol?
    private var interactor: ProfileInteractorProtocol
    private var wireframe: ProfileRouterProtocol
    
    init(view: ProfileViewProtocol) {
        self._view = view
        self.interactor = ProfileInteractor()
        self.wireframe = ProfileRouter()
    }
}

// MARK: - extending ProfilePresenter to implement it's protocol
extension ProfilePresenter: ProfilePresenterProtocol {
    
}
