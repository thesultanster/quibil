//
//  Item.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/25/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation

class Item {
    var categoryId: String
    var id: String
    var name: String
    var price: Double
    var restaurantId: String
    
    var addOns: [FoodItemAddOn]?
    
    init(id: String, name: String, price: Double, categoryId: String, restaurantId: String) {
        self.id = id
        self.name = name
        self.price = price
        self.categoryId = categoryId
        self.restaurantId = restaurantId
    }
    
    convenience init?(dic: [String:Any]) {
        
        // Get item info from dic
        guard let id = dic["id"] as? String else {
            print("No Id for item")
            return nil
        }
        guard let categoryId = dic["categoryId"] as? String else {
            print("No categoryId for item")
            return nil
        }
        guard let name = dic["name"] as? String else {
            print("No name for item")
            return nil
        }
        guard let price = dic["price"] as? Double else {
            print("No price for item")
            return nil
        }
        guard let restaurantId = dic["restaurantId"] as? String else {
            print("No restaurantId for item")
            return nil
        }
        
        // Initalize required fields
        self.init(id: id, name: name, price: price / 100, categoryId: categoryId, restaurantId: restaurantId)
        
        // Load any addOns
        if let addOnsDic = dic["addOns"] as? [String:Any] {
            self.addOns = []
            for addOnData in addOnsDic.values {
                if let addOnDic = addOnData as? [String:Any] {
                    guard let addOn = FoodItemAddOn(dic: addOnDic) else {
                        continue
                    }
                    self.addOns?.append(addOn)
                }
            }
        }
    }
}
