//
//  FoodItemAddOn.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 7/31/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation

class FoodItemAddOn {
    
    var id: String
    var name: String
    var price: Double
    
    
    
    init(id: String, name: String, price: Double) {
        self.id = id
        self.name = name
        self.price = price
    }
    
    convenience init?(dic: [String:Any]) {
        guard let id = dic["id"] as? String else {
            return nil
        }
        guard let name = dic["name"] as? String else {
            return nil
        }
        guard let price = dic["price"] as? Double else {
            return nil
        }
        self.init(id: id, name: name, price: price / 100)
    }
}
