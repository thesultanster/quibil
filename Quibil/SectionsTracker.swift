//
//  SectionsTracker.swift
//  Quibil
//
//  Created by Michael Ryan Santos Villanueva on 9/17/18.
//  Copyright © 2018 Quibil. All rights reserved.
//

import Foundation


class SectionsTracker {
    var sections: [SectionTrackerSection] = []
    func index(section: Int, row: Int) -> Int? {
        if section < sections.count && row < sections[section].rows {
            return row + sections[section].startIndex
        }
        return nil
    }
    func title(of section: Int) -> String? {
        if section < 0 || section > sections.count { return nil }
        else { return sections[section].title }
    }
}

class SectionTrackerSection {
    var title: String?
    var rows: Int = 0
    var startIndex: Int
    init(title: String?, startIndex: Int, rows: Int) {
        self.title = title
        self.rows = rows
        self.startIndex = startIndex
    }
}
