//
//  AUTTOTPDelegate.h
//  TwilioAuth
//
//  Created by Adriana Pineda on 6/5/17.
//  Copyright © 2017 Authy. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Represents the AUTTOTPDelegate protocol
 */
@protocol AUTTOTPDelegate <NSObject>

@required
/**
 Receives the current TOTP
 @param totp the TOTP of the current app
 @param error an empty error of type NSError if the opration was successful or a non-empty NSError if it wasn't
 */
- (void)didReceiveTOTP:(NSString * _Nullable)totp withError:(NSError * _Nullable)error;

@end
