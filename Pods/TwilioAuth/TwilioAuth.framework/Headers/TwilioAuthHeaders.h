//
//  TwilioAuth.h
//  TwilioAuth
//
//  Created by Adriana Pineda on 3/8/16.
//  Copyright 2011-2017 Twilio, Inc.
//
//  All rights reserved. Use of this software is subject to the terms and conditions of the
//  Twilio Terms of Service located at http://www.twilio.com/legal/tos
//

#import <UIKit/UIKit.h>

//! Project version number for TwilioAuth.
FOUNDATION_EXPORT double TwilioAuthVersionNumber;

//! Project version string for TwilioAuth.
FOUNDATION_EXPORT const unsigned char TwilioAuthVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TwilioAuth/PublicHeader.h>
#import <TwilioAuth/TwilioAuth.h>
